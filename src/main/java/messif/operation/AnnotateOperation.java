/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation;

import messif.algorithm.AlgorithmMethodException;
import messif.data.DataObject;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.answer.RankedAnswer;
import messif.operation.params.ApproximateSearch;
import messif.operation.params.DataObjects;
import messif.operation.params.KNN;
import messif.operation.params.QueryObject;
import messif.operation.processing.OperationEvaluator;
import messif.operation.processing.impl.QueueOperationProcessor;
import messif.operation.search.CheckNearDupsOperation;
import messif.operation.search.KNNOperationApprox;
import messif.operation.search.RangeOperationApprox;
import messif.record.ModifiableRecordImpl;
import messif.utility.ProcessingStatus;
import messif.utility.SortedCollection;
import messif.utility.proxy.annotations.Get;

import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Operation for annotating given object(s) by kNN-based annotation.
 */
public interface AnnotateOperation extends ProcessObjectsOperation, KNN, ApproximateSearch {

    String TYPE_STRING = "annotate";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }


    String FIELD_PARAM = "field_name";

    @Get(field = FIELD_PARAM)
    String getFieldName();


    String DIST_INFLUENCE_PARAM = "distance_influence";

    /**
     * Returns the parameter saying the influence of the query-object distance on the influence of the keyword.
     * @return influence of the query-object distance.
     */
    @Get(field = DIST_INFLUENCE_PARAM)
    float getDistInfluence(float defaultDistInfl);

    default float getDistInfluence() {
        return getDistInfluence(0.5f);
    };


    String DIST_MAX_PARAM = "maximum_distance";

    /**
     * Returns the maximum distance of objects (images) to be taken into account for the annotation.
     * @return maximum distance
     */
    @Get(field = DIST_MAX_PARAM)
    float getMaxDist(float defaultMax);

    default float getMaxDist() {
        return getMaxDist(100f);
    };


    String THRESHOLD_PARAM = "score_threshold";

    /**
     * Returns the threshold of confidence of given keyword; we use threshold * K.
     * @return keyword confidence threshold
     */
    @Get(field = THRESHOLD_PARAM)
    float getThreshold(float defaultThresh);

    default float getThreshold() {
        return getThreshold(0.1f);
    };


    String REMOVE_EXACT = "remove_near_dups";

    /**
     * Flag if we should remove the exact match (for testing purposes)
     */
    @Get(field = REMOVE_EXACT)
    boolean removeNearDups(boolean removeNearDups);

    default boolean removeNearDups() {
        return removeNearDups(false);
    }

    /**
     * Helper that
     */
    class Helper extends ProcessObjectsOperation.Helper<AnnotateOperation, ProcessObjectsAnswer> {

        protected final String fieldName;

        /**
         * Creates this processor given the operation to be processed.
         *
         * @param operation         operation to be executed
         */
        public Helper(AnnotateOperation operation) {
            super(operation, ProcessObjectsAnswer.class, ProcessObjectsAnswer.SOME_OBJECTS_PROCESSED,
                    new ProcessingStatus[] { ProcessObjectsAnswer.OK_PROCESSED, AbstractAnswer.OPERATION_OK},
                    new ProcessingStatus[] { ProcessObjectsAnswer.ERROR_PROCESSING, ProcessObjectsAnswer.SOME_OBJECTS_PROCESSED, ProcessObjectsAnswer.QUERY_OBJ_ERROR });
            fieldName = operation.getFieldName();
        }

        public boolean annotate(DataObject queryObject, RankedAnswer kNNAnswer) {
            if (kNNAnswer.getAnswerCount() <= 0) {
                skipObject(queryObject, kNNAnswer);
                return false;
            }
            Map<String, Float> keywordScores = new HashMap<>();
            float maxDistance = operation.getMaxDist() * (1f/ operation.getDistInfluence());
            float [] distances = kNNAnswer.getAnswerDistances();

            // for testing purposes, remove the near-duplicate images
            int i = 0;
            if (operation.removeNearDups()) {
                float nearDupsThresh = operation.getMaxDist() / 20f;
                while (i < kNNAnswer.getAnswerCount() && distances[i] <= nearDupsThresh) {
                    i++;
                }
                //distances = Arrays.copyOfRange(distances, i, distances.length);
            }
            int objectToUse = distances.length - i;

            // iterate over the kNN objects
            for ( ; i < kNNAnswer.getAnswerCount(); i++) {
                float objInfluence = Math.max(0f, (maxDistance - distances[i]) / maxDistance);
                String [] kws = kNNAnswer.getAnswerObjects()[i].getField(fieldName, String [].class);
                if (kws == null) {
                    continue;
                }
                for (String kw : kws) {
                    if (keywordScores.putIfAbsent(kw, objInfluence) != null) {
                        keywordScores.put(kw, objInfluence + keywordScores.get(kw));
                    }
                }
            }

            // use threshold and sort the keywords by confidence
            SortedCollection<Map.Entry<String, Float>> sorted = new SortedCollection<>(keywordScores.size(), comparator);
            float threshold = operation.getThreshold() * objectToUse;
            sorted.addAll(keywordScores.entrySet().stream().filter(entry -> entry.getValue() >= threshold).collect(Collectors.toList()));

            // put the object with keywords to the answer
            objectProcessed(DataObject.addField(queryObject, fieldName, sorted.stream().map(Map.Entry::getKey)
                            .collect(Collectors.toList()).toArray(new String [sorted.size()])), AbstractAnswer.OPERATION_OK);
            return true;
        }

        private final static Comparator<Map.Entry<String, Float>> comparator = new Comparator<Map.Entry<String, Float>>() {
            @Override
            public int compare(Map.Entry<String, Float> o1, Map.Entry<String, Float> o2) {
                return Float.compare(o2.getValue(), o1.getValue());
            }
        };

    }

    /**
     * A processor that takes the operation and runs single {@link RangeOperationApprox} operations on a given
     *  algorithm. The overall {@link CheckNearDupsOperation} operation's answer is composed based on the individual
     *  results.
     */
    class Processor extends QueueOperationProcessor<AnnotateOperation, ProcessObjectsAnswer, KNNOperationApprox> {

        private final OperationEvaluator algorithmToProcess;
        final Helper helper;
        protected final String [] returnFields;

        public Processor(AnnotateOperation operation, OperationEvaluator evaluator) {
            super(operation);
            this.algorithmToProcess = evaluator;
            this.helper = new Helper(operation);
            this.returnFields = new String[] {DataObject.ID_FIELD, operation.getFieldName()};
        }


        @Override
        protected void initProcessor() throws AlgorithmMethodException {
            super.initProcessor();

            // create a range query operation for each of the data object
            for (DataObject queryObject : operation.getObjects()) {
                final ModifiableRecordImpl singleOpParams = new ModifiableRecordImpl(operation.getParams());
                singleOpParams.removeField(DataObjects.OBJECTS_FIELD);
                singleOpParams.setField(QueryObject.QUERY_OBJECT_FIELD, queryObject);
                singleOpParams.setField(ReturnDataOperation.FIELDS_TO_RETURN_FIELD, returnFields);

                KNNOperationApprox singleOp = OperationBuilder.build(singleOpParams, KNNOperationApprox.class, false);
                addProcessingItem(singleOp);
            }
            queueClose();
        }

        @Override
        protected void processItem(KNNOperationApprox processingItem) throws AlgorithmMethodException {
            try {
                final AbstractAnswer singleAnswer = algorithmToProcess.evaluate(processingItem);
                if (! (singleAnswer instanceof RankedAnswer && ((RankedAnswer) singleAnswer).getAnswerCount() > 0)) {
                    helper.skipObject(processingItem.getQueryObject(), singleAnswer);
                } else {
                    helper.annotate(processingItem.getQueryObject(), (RankedAnswer) singleAnswer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Nullable
        @Override
        public ProcessObjectsAnswer finish() {
            return helper.getAnswer();
        }
    }

}
