/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation;

/**
 * This operation registers all {@link GetCandidateSetOperation}s processed by current matchers and
 * it merges their results. Specifically, the sub-candidate operations are created so that they all
 * share the same {@link GetCandidateSetOperation#candidateSetLocators} queue and this operation is
 * finished only when all sub-candidate operations finish.
 */
public class MergingGetCandidatesOperation {
//    extends GetCandidateSetOperation {
//
//    /**
//     * List of sub-candidate operations.
//     */
//    final Collection<GetCandidateSetOperation> candSetOperations;
//
//    /**
//     * Creates this super-candidate operation.
//     */
//    public MergingGetCandidatesOperation(List<GetCandidateSetOperation> subOperations, BlockingQueue<String> candidateLocatorQueue) {
//        super(null, 0, candidateLocatorQueue);
//        candSetOperations = Collections.unmodifiableList(subOperations);
//    }
//
//    @Override
//    public int getCandidateSetSize() {
//        int retVal = 0;
//        for (GetCandidateSetOperation subOp : candSetOperations) {
//            retVal += subOp.getCandidateSetSize();
//        }
//        return retVal;
//    }
//
//    @Override
//    public boolean isFinished() {
//        for (GetCandidateSetOperation op : candSetOperations) {
//            if (!op.isFinished()) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    public Collection<GetCandidateSetOperation> getCandSetOperations() {
//        return candSetOperations;
//    }
//
//    public static BlockingQueue<String> createCandLocQueue() {
//        return new DeduplicatingBlockingQueue<>();
//    }
//
//    protected static class DeduplicatingBlockingQueue<E> extends LinkedBlockingQueue<E> {
//
//        private final Set<E> seenObjects = new HashSet<>();
//
//        @Override
//        public E poll() {
//            E object = super.poll();
//            if (object == null || seenObjects.add(object)) {
//                return object;
//            }
//            return poll();
//        }
//
//        @Override
//        public E poll(long timeout, TimeUnit unit) throws InterruptedException {
//            E object = super.poll(timeout, unit);
//            if (object == null || seenObjects.add(object)) {
//                return object;
//            }
//            return poll(timeout, unit);
//        }
//
//        @Override
//        public E take() throws InterruptedException {
//            E object = super.take();
//            if (object == null || seenObjects.add(object)) {
//                return object;
//            }
//            return take();
//        }
//
//        @Override
//        public E peek() {
//            E object = super.peek();
//            if (object == null || ! seenObjects.contains(object)) {
//                return object;
//            }
//            return peek();
//        }
//
//    }
}
