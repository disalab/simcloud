/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.distance.CosDistanceSparse;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.utility.ArrayMap;
import messif.utility.json.JSONWriter;

import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created by david on 13/12/16.
 */
public class SparseVectorPreprocessor extends AddFieldProcessor {

    private static final String [] newFieldKeys =
            new String [] {CosDistanceSparse.POSITIONS_FIELD, CosDistanceSparse.VALUES_FIELD, CosDistanceSparse.MAGNITUDE_FIELD};

    protected SparseVectorPreprocessor(String requiredField, String outputField, boolean removeSourceField) {
        super(requiredField, outputField, removeSourceField);
    }

    @Override
    protected Object getFieldValue(DataObject record) {
        if (! (record.getField(requiredField) instanceof String)) {
            Logger.getLogger(KeywordsPreprocessor.class.getName()).warning("the following record does not contain a String field " + requiredField + " with sparse vector: " + JSONWriter.writeToJSON(record, false));
            return record;
        }
        if (!(record instanceof ModifiableRecord)) {
            record = new ModifiableRecordImpl(record);
        }
        String[] stringList = parseStringList((String) record.getField(requiredField), "\\s");
        IntFloat[] pairsToSort = new IntFloat[stringList.length / 2];

        int i = 0;
        // then the first value is the object "_id"
        if (stringList.length % 2 == 1) {
            if (record.getID() == null) {
                ((ModifiableRecord) record).setField(DataObject.ID_FIELD, stringList [i ++]);
            }
        }
        for ( ; i < stringList.length; i += 2) {
            pairsToSort[i / 2] = new IntFloat(stringList[i], stringList[i + 1]);
        }
        Arrays.sort(pairsToSort, null);

        final int[] positions = new int[pairsToSort.length];
        final float[] values = new float[pairsToSort.length];
        for (int j = 0; j < pairsToSort.length; j++) {
            positions[j] = pairsToSort[j].position;
            values[j] = pairsToSort[j].value;
        }
        final float magnitude = calculateMagnitude(values);

        return new ArrayMap<>(newFieldKeys, new Object[]{positions, values, magnitude}, true);
    }

    public static String[] parseStringList(String line, String separator) {
        line = line.trim();
        if (line.length() == 0) {
            return new String[0];
        }
        return line.split("\\s*" + separator + "\\s*");
    }

    private float calculateMagnitude(float [] values) {
        double sum = 0d;
        for (float value : values) {
            sum += value * value;
        }
        return (float) Math.sqrt(sum);
    }

    protected static class IntFloat implements Comparable<IntFloat> {

        protected final int position;
        protected final float value;

        public IntFloat(String positionStr, String valueStr) {
            position = Integer.valueOf(positionStr);
            value = Float.valueOf(valueStr);
        }

        public IntFloat(int position, float value) {
            this.position = position;
            this.value = value;
        }

        @Override
        public int compareTo(IntFloat o) {
            return Integer.compare(position, o.position);
        }
    }
//
//    public ObjectSparseVector(TIntFloatMap map, boolean normalize) throws EOFException, IOException, NumberFormatException {
//        float normalization = normalize ? calculateMagnitude(map.values()) : 1f;
//
//        IntFloat[] pairsToSort = new IntFloat[map.size()];
//        int j = 0;
//        for (TIntFloatIterator iterator = map.iterator(); iterator.hasNext();) {
//            iterator.advance();
//            pairsToSort[j ++] = new IntFloat(iterator.key(), iterator.value() / normalization);
//        }
//        Arrays.sort(pairsToSort, null);
//
//        this.positions = new int[pairsToSort.length];
//        this.values = new float[pairsToSort.length];
//        for (int i = 0; i < pairsToSort.length; i++) {
//            positions[i] = pairsToSort[i].position;
//            values[i] = pairsToSort[i].value;
//        }
//    }
//
//    public ObjectSparseVector(ObjectSparseVector copyFrom, boolean normalize) throws EOFException, IOException, NumberFormatException {
//
//        this.positions = Arrays.copyOf(copyFrom.positions, copyFrom.positions.length);
//        if (! normalize) {
//            this.values = Arrays.copyOf(copyFrom.values, copyFrom.values.length);
//        } else {
//            float normalization = calculateMagnitude(copyFrom.values);
//            this.values = new float [copyFrom.values.length];
//            for (int i = 0; i < positions.length; i++) {
//                values[i] = copyFrom.values[i] / normalization;
//            }
//        }
//    }
//
//    protected ObjectSparseVector(String locator, int [] positions, float [] values) {
//        super(locator);
//        this.positions = positions;
//        this.values = values;
//    }

}
