/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.distance.L2DistanceSparseCNN;
import messif.utility.ArrayMap;
import messif.utility.json.JSONWriter;

import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created by david on 13/12/16.
 */
public class SparseCNNVectorPreprocessor extends AddFieldProcessor {

    private static final String [] newFieldKeys =
            new String [] {L2DistanceSparseCNN.ZEROBITS_FIELD, L2DistanceSparseCNN.NONZEROS_FIELD, L2DistanceSparseCNN.LENGTH_FIELD};

    public SparseCNNVectorPreprocessor(String requiredField, String outputField, boolean removeSourceField) {
        super(requiredField, outputField, removeSourceField);
    }

    @Override
    protected Object getFieldValue(DataObject record) {
        if (! (record.getField(requiredField) instanceof float [])) {
            Logger.getLogger(KeywordsPreprocessor.class.getName()).warning("the following record does not contain a float [] field " + requiredField + " with CNN sparse vector: " + JSONWriter.writeToJSON(record, false));
            return record;
        }
        float [] rawData = (float []) record.getField(requiredField);

        int dataLength = rawData.length;
        int [] zeroBits = new int [ (int) Math.ceil((float) rawData.length / (float) (L2DistanceSparseCNN.BIT_COMPONENT_SIZE))];
        SimpleFloatArray nonZeros = new SimpleFloatArray(rawData.length / 2);

        for (int i = 0; i < zeroBits.length; i++) {
            int nextIntBits =  (i + 1) * L2DistanceSparseCNN.BIT_COMPONENT_SIZE <= dataLength ?
                    L2DistanceSparseCNN.BIT_COMPONENT_SIZE : dataLength - i * L2DistanceSparseCNN.BIT_COMPONENT_SIZE;
            for (int j = 0; j < nextIntBits; j++) {
                if (rawData[i * L2DistanceSparseCNN.BIT_COMPONENT_SIZE + j] == 0f)
                    zeroBits[i] |= 1 << j;
                else
                    nonZeros.add(rawData[i * L2DistanceSparseCNN.BIT_COMPONENT_SIZE + j]);
            }
        }

        return new ArrayMap<>(newFieldKeys, new Object[]{zeroBits, nonZeros.toArray(), dataLength}, true);
    }

    /** Simple encapsulation of a variable-size array of floats. Only the essential methods implemented. */
    private static class SimpleFloatArray {
        private static final float INCREASE_FACTOR = 1.3f;

        /** Actual float data. */
        float [] data;

        /** Current size of the array. */
        int size = 0;

        public SimpleFloatArray(int initialSize) {
            data = new float [initialSize];
        }

        public void add(float value) {
            // increase the storage size
            if (data.length <= size) {
                float [] newData = new float [(int) (((float) data.length) * INCREASE_FACTOR)];
                System.arraycopy(data, 0, newData, 0, data.length);
                data = newData;
            }
            data[size ++] = value;
        }

        public float [] toArray() {
            return Arrays.copyOf(data, size);
        }
    }

}
