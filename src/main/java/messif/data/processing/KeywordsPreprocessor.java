/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.distance.IntJaccardDistanceFunction;
import messif.record.Record;
import messif.record.processing.RecordProcessor;
import messif.record.processing.RecordProcessors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;


/**
 * A preprocessor that takes a list of keywords (tags), replaces it with a preprocessed list of these words
 *   (lower cased, sorted, unique) and creates another field with a list of the integer hash values of these words.
 */
public class KeywordsPreprocessor {

    /**
     * Create a new processor given the source field, output field and flag about removing the source field.
     */
    public static RecordProcessor<DataObject> create(String tagsField) {
        return create(tagsField, IntJaccardDistanceFunction.DEFAULT_HASHCODE_FIELD);
    }

    /**
     * Create a new processor given the source field, output field and flag about removing the source field.
     */
    public static RecordProcessor<DataObject> create(String tagsField, String tagsHashField) {
        return RecordProcessors.chainProcessors(new CleanTagsProcessor(tagsField), new AddTagHashesProcessor(tagsField, tagsHashField));
    }

    /**
     * Create a new processor given the source field, output field and flag about removing the source field.
     */
    public static RecordProcessor<DataObject> createRich(String titleField, String tagsField) {
        return createRich(titleField, tagsField, IntJaccardDistanceFunction.DEFAULT_HASHCODE_FIELD);
    }

    /**
     * Create a new processor given the source field, output field and flag about removing the source field.
     */
    public static RecordProcessor<DataObject> createRich(String titleField, String tagsField, String tagsHashField) {
        return RecordProcessors.chainProcessors(
                new AltTagFieldProcessor(titleField, tagsField),
                new ReplaceTagsRichProcessor(tagsField),
                new AddTagHashesProcessor(tagsField, tagsHashField)
        );
    }

    /**
     * Creates a simple processor that does not change the 'tags' field but only adds the tag_hash field
     */
    public static RecordProcessor<DataObject> createAddHash(String tagsField) {
        return new AddTagHashesProcessor(tagsField, IntJaccardDistanceFunction.DEFAULT_HASHCODE_FIELD);
    }

    /**
     * Helper method that returns a list of string keywords stored in a given field, but it can also convert
     * 1) single string to list and 2) tags with probability to just list of tags.
     */
    protected static String [] getKeywordField(DataObject record, String fieldName) {
        Object field = record.getField(fieldName);
        if (field == null || field instanceof String []) {
            return (String []) field;
        }
        if (field instanceof String) {
            return new String [] {(String) field};
        }
        try {
            String [] retVal = new String [((Object []) field).length];
            int i = 0;
            for (Object nameProb : (Object[]) field) {
                retVal[i] = (String) ((nameProb instanceof Map) ? ((Map) nameProb).get("name") : ((Record) nameProb).getField("name"));
                i++;
            }
            return retVal;
        } catch (ClassCastException ex) {
            return null;
        }
    }

    /**
     * Create a new processor given the source field, output field and flag about removing the source field.
     */
    public static RecordProcessor<DataObject> mergingProcessor(String tagsField, String ... fieldsToAdd) {
        RecordProcessor [] processors = new RecordProcessor[fieldsToAdd.length + 2];
        int i = 0;
        for (; i < fieldsToAdd.length; i++) {
            processors[i] = new AddToTagsProcessor(fieldsToAdd[i], tagsField);
        }
        processors[i ++] = new ReplaceTagsRichProcessor(tagsField);
        processors[i ++] = new AddTagHashesProcessor(tagsField, IntJaccardDistanceFunction.DEFAULT_HASHCODE_FIELD);
        return RecordProcessors.chainProcessors(processors);
    }

    /**
     * If the field "tags" does not exist, take another field instead.
     */
    protected static class AltTagFieldProcessor extends AddFieldProcessor {

        protected AltTagFieldProcessor(String titleField, String tagField) {
            super(titleField, tagField);
            if (titleField == null) {
                throw new IllegalArgumentException("the required field cannot be null for " + AltTagFieldProcessor.class.getSimpleName());
            }
        }

        @Override
        protected Object getFieldValue(DataObject record) {
            return getKeywordField(record, requiredField);
        }

        @Override
        public boolean preCondition(DataObject record) {
            return super.preCondition(record) && record.getField(outputField) == null;
        }
    }

    /**
     * If the field "tags" does not exist, take another field instead.
     */
    protected static class AddToTagsProcessor extends AddFieldProcessor {

        protected AddToTagsProcessor(String fieldToAdd, String tagField) {
            super(fieldToAdd, tagField);
            if (fieldToAdd == null) {
                throw new IllegalArgumentException("the field to add cannot be null for " + AddToTagsProcessor.class.getSimpleName());
            }
        }

        @Override
        protected Object getFieldValue(DataObject record) {
            String [] dataToAdd = getKeywordField(record, requiredField);
            String [] existingTags = getKeywordField(record, outputField);
            if (dataToAdd == null)
                return existingTags;
            if (existingTags == null)
                return Arrays.copyOf(dataToAdd, dataToAdd.length);

            String [] enrichedField = Arrays.copyOf(existingTags, dataToAdd.length + existingTags.length);
            System.arraycopy(dataToAdd, 0, enrichedField, existingTags.length, dataToAdd.length);
            return enrichedField;
        }
    }

    /**
     * This processor replaces the "tags" field with a cleared field.
     */
    protected static class CleanTagsProcessor extends ReplaceFieldProcessor {

        /**
         * Create a new processor given the field to be replaced.
         *
         * @param fieldToReplace
         */
        protected CleanTagsProcessor(String fieldToReplace) {
            super(fieldToReplace);
        }

        @Override
        protected Object getFieldValue(DataObject record) {
            return sortUnique(basicPreprocessing(getKeywordField(record, requiredField)));
        }

        /**
         * Lower case all the strings, remove empty strings, sort them and unique.
         * @param origStrings
         * @return preprocessed strings
         */
        protected static String [] basicPreprocessing(String [] origStrings) {
            String [] strings = new String [origStrings.length];
            for (int i = 0; i < strings.length; i++) {
                strings[i] = origStrings[i].toLowerCase();
            }
            return strings;
        }

        /**
         * Lower case all the strings, remove empty strings, sort them and unique.
         * @param strings
         * @return preprocessed strings
         */
        protected static String [] sortUnique(String [] strings) {
            Arrays.sort(strings);

            String emptyStr = "".intern();
            String previous = null;
            int removed = 0;
            for (int i = 0; i < strings.length - removed; ) {
                String string = strings[i];
                if (string == null || string.equals(previous) || emptyStr.equals(string)) {
                    // remove the string
                    System.arraycopy(strings, i + 1, strings, i, strings.length - i - removed - 1);
                    removed ++;
                } else {
                    i++;
                }
                previous = string;
            }
            if (removed > 0) {
                return Arrays.copyOf(strings, strings.length - removed);
            } else {
                return strings;
            }
        }
    }

    /**
     * This does a much richer tags preprocessing than {@link CleanTagsProcessor}: <ul>
     *     <li>remove given stop words: creative_tag:no, creative_tag:yes</li>
     *     <li>split the 'words' by space, (semi)colon, and comma</li>
     * </ul>
     */
    protected static class ReplaceTagsRichProcessor extends CleanTagsProcessor {

        /**
         * Create a new processor given the field to be replaced.
         *
         * @param fieldToReplace
         */
        protected ReplaceTagsRichProcessor(String fieldToReplace) {
            super(fieldToReplace);
        }

        @Override
        protected Object getFieldValue(DataObject record) {
            return sortUnique(richProcessing(basicPreprocessing(getKeywordField(record, requiredField))));
        }

        static Predicate<String>[] stopWords = new Predicate [] {"creative_tag:no"::equals, "creative_tag:yes"::equals};

        static Pattern pattern = Pattern.compile("\\s*[\\s,;:]\\s*");

        /**
         * Split the strings by " ", ";" and ","
         * @param origStrings
         * @return preprocessed strings
         */
        public static String [] richProcessing(String [] origStrings) {
            List<String> strings = new ArrayList<>(origStrings.length);
            for (int i = 0; i < origStrings.length; i++) {
                final String word = origStrings[i];
                if (Arrays.stream(stopWords).anyMatch(f -> f.test(word))) {
                    continue;
                }
                strings.addAll(Arrays.asList(pattern.split(word)));
            }
            return strings.toArray(new String [strings.size()]);
        }
    }

    /**
     * Adds a "hashcodes" field created from given tags.
     */
    protected static class AddTagHashesProcessor extends AddFieldProcessor {

        protected AddTagHashesProcessor(String requiredField, String outputField) {
            super(requiredField, outputField);
        }

        @Override
        protected Object getFieldValue(DataObject record) {
            return getHashCodes(getKeywordField(record, requiredField));
        }

        /**
         * Returns a sorted list of hash codes of given words. THE RESULT CAN CONTAIN TWO INTEGERS TWICE
         * @param words
         * @return a sorted list of hash codes of given words. THE RESULT CAN CONTAIN TWO INTEGERS TWICE
         */
        public static int [] getHashCodes(String [] words) {
            int [] retVal = new int [words.length];
            for (int i = 0; i < retVal.length; i++) {
                retVal[i] = words[i].hashCode();
            }
            Arrays.sort(retVal);
            // unique
            int previous = -1;
            for (int number : retVal) {
                if (number == previous) {
                    retVal = Arrays.stream(retVal).distinct().toArray();
                    break;
                }
                previous = number;
            }

            return retVal;
        }
    }

}
