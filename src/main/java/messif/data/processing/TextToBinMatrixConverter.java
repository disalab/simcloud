/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A one-time converter of text representation of float matrix to a Java serialization. Names
 *  of the files are hard-coded. TODO
 * @author david
 */
public class TextToBinMatrixConverter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float[][] matrix = new float[4096][];

        // read in the text representation of the 4096 \times x float matrix
        String textFile = "men-matrix-java.txt";
        try (BufferedReader in = new BufferedReader(new FileReader(textFile))) {
            int i = 0;
            while (in.ready() && i < 4096) {
                String line = in.readLine();
                matrix[i++] = parseFloatVector(line);
            }
            System.out.println("# of lines read in: " + i);
        } catch (IOException ex) {
            Logger.getLogger(TextToBinMatrixConverter.class.getName()).log(Level.SEVERE, null, ex);
        }

        // print it out as a Java serialized float [] []
        String file = "men-matrix-java.bin";
        try (ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            out.writeUnshared(matrix);
        } catch (IOException ex) {
            Logger.getLogger(TextToBinMatrixConverter.class.getName()).log(Level.SEVERE, null, ex);
        }

        // just check the size of the matrix by reading the binary representation
        try (ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            matrix = (float[][]) in.readObject();
            System.out.println("matrix length: " + matrix.length);
            System.out.println("matrix width: " + matrix[0].length);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(TextToBinMatrixConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static float[] parseFloatVector(String line) throws NumberFormatException, EOFException {
        if (line == null)
            throw new EOFException();
        line = line.trim();
        if (line.length() == 0)
            return new float[0];
        String[] numbers = line.split(line.indexOf(',') != -1 ? "\\s*,\\s*" : "\\s+");
        return convertFloatVector(numbers, 0, numbers.length);
    }

    /**
     * Converts the given array of float strings to a vector of floats.
     *
     * @param numbers the array of float strings to convert
     * @param offset the starting index in the {@code numbers array}
     * @param length the number of {@code numbers} to convert starting from {@code offset}
     * @return the converted vector of floats
     * @throws NumberFormatException if the given {@code numbers} contained a value that cannot be converted to float
     * @throws IndexOutOfBoundsException if the given offset or length is not valid for the {@code numbers} array
     */
    public static float[] convertFloatVector(String[] numbers, int offset, int length) throws NumberFormatException, IndexOutOfBoundsException {
        float[] data = new float[length];
        for (int i = 0; i < length; i++)
            data[i] = Float.parseFloat(numbers[i + offset]);
        return data;
    }

}
