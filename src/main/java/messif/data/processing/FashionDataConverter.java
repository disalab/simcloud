/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;


import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Given a float matrix, this class converts a neural network Caffe descriptor to lower dimension
 *  by multiplying it with the matrix.
 * @author david
 */
public class FashionDataConverter extends PCAConverter {
    /**
     * Creates new converted given a file with serialized float matrix to be used.
     * @param matrixFile name of file (with full or relative path) with serialized float [] [] matrix
     * @throws IOException if the file is not readable or is corrupted
     * @throws ClassNotFoundException if the matrix cannot be deserialized
     */
    public FashionDataConverter(String matrixFile, String requiredField, String outputField, boolean substitute) throws InstantiationException {
        this(readMatrix(matrixFile), requiredField, outputField, substitute);
    }

    public FashionDataConverter(float [] [] matrix, String requiredField, String outputField, boolean substitute) throws InstantiationException {
        super(matrix, requiredField, outputField, substitute);
    }

    protected static float [] []readMatrix(String matrixFile) throws  InstantiationException {
        try (ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(matrixFile)))) {
            float [] [] matrix = (float[][]) in.readObject();
            Logger.getLogger(FashionDataConverter.class.getName()).log(Level.INFO, "read a float [] [] matrix to convert caffe vectors. Matrix size: {0} x {1}", new Object[]{matrix.length, matrix[0].length});
            return matrix;
        } catch (IOException | ClassNotFoundException ex) {
            throw new InstantiationException(ex.toString());
        }
    }

}
