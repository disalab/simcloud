/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance;


import messif.distance.impl.JaccardDistanceFunction;
import messif.utility.PeekIterator;
import messif.utility.PeekIteratorImpl;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Jaccard distance function to be applied on integer hash codes created from tags (words).
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class IntJaccardDistanceFunction extends JaccardDistanceFunction<Integer, int[]> {

    public static final String DEFAULT_HASHCODE_FIELD = "_tags_hashes";

    //public static final String TERM_FIELD = "_terms";

    /** Class id for serialization. */
    private static final long serialVersionUID = 1L;

    public IntJaccardDistanceFunction() {
        super(Integer::compareTo);
    }

    @Override
    protected PeekIterator<Integer> getValuesIterator(int[] obj) {
        //return new PeekIteratorImpl<>(new IntIterator((int[]) obj.get(HASHCODE_FIELD)));
        return new PeekIteratorImpl<>(new IntIterator(obj));
    }

    @Override
    public Class<int[]> getObjectClass() {
        return int[].class;
    }

    static class IntIterator implements Iterator<Integer> {

        private final int[] array;

        protected IntIterator(int[] array) {
            this.array = array;
        }

        private int index;

        @Override
        public boolean hasNext() {
            return index < array.length;
        }

        @Override
        public Integer next() {
            if (index >= array.length)
                throw new NoSuchElementException();
            return array[index++];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }


}
