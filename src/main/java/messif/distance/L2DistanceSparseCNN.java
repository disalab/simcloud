/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance;

import java.util.Map;

/**
 * @author david
 */
public class L2DistanceSparseCNN implements DistanceFunc<Map> {

    public static final String ZEROBITS_FIELD = "_bitvector";

    public static final String NONZEROS_FIELD = "_nonzerovalues";

    public static final String LENGTH_FIELD = "_length";

    public static final int BIT_COMPONENT_SIZE = Integer.BYTES * 8;

    @Override
    public float getDistance(Map o1, Map o2, float threshold) {
        int dataLength = (int) o1.get(LENGTH_FIELD);
        if (dataLength != (int) o2.get(LENGTH_FIELD)) {
            throw new DistanceException("sparse CNN vectors have different lenghts: " + dataLength + " and " + (int) o2.get(LENGTH_FIELD));
        }

        int [] zeroBits = (int []) o1.get(ZEROBITS_FIELD);
        float [] nonZeros = (float []) o1.get(NONZEROS_FIELD);

        int [] zeroBits2 = (int []) o2.get(ZEROBITS_FIELD);
        float [] nonZeros2 = (float []) o2.get(NONZEROS_FIELD);

        float powSum = 0f;
        float dif;
        // index within the non-zero values in both objects
        int nonZeroIndex = 0;
        int nonZeroIndex2 = 0;

        int i = 0;
        int zeroWordIndex = 0;
        while (i < dataLength) {
            int currentZerosWord = (zeroWordIndex >= zeroBits.length) ? 0 : zeroBits[zeroWordIndex];
            int currentZerosWord2 = (zeroWordIndex >= zeroBits2.length) ? 0 : zeroBits2[zeroWordIndex];
            int j = BIT_COMPONENT_SIZE;
            int currentMask = 1;
            while (j-- > 0 && i < dataLength) {
                if ((currentZerosWord & currentMask) == 0) {
                    if ((currentZerosWord2 & currentMask) == 0) {
                        dif = nonZeros[nonZeroIndex++] - nonZeros2[nonZeroIndex2++];
                    } else {
                        dif = nonZeros[nonZeroIndex++];
                    }
                    powSum += dif * dif;
                } else {
                    if ((currentZerosWord2 & currentMask) == 0) {
                        dif = nonZeros2[nonZeroIndex2++];
                        powSum += dif * dif;
                    }
                }
                currentMask = currentMask << 1;
                i++;
            }
            zeroWordIndex++;
        }
        return (float) Math.sqrt(powSum);
    }

    @Override
    public Class<Map> getObjectClass() {
        return Map.class;
    }


//
//    public CosDistanceSparse(BufferedReader stream) throws EOFException, IOException, NumberFormatException {
//        super(stream);
//        magnitude = calculateMagnitude();
//    }
//
//    public CosDistanceSparse(TIntFloatMap map, boolean normalize) throws EOFException, IOException, NumberFormatException {
//        super(map, normalize);
//        magnitude = normalize ? 1f : calculateMagnitude();
//    }
//
//    public CosDistanceSparse(ObjectSparseVector copyFrom, boolean normalize) throws EOFException, IOException, NumberFormatException {
//        super(copyFrom, normalize);
//        magnitude = normalize ? 1f : calculateMagnitude();
//    }
//
//    protected CosDistanceSparse(String locator, int[] positions, float[] values, float magnitude) {
//        super(locator, positions, values);
//        this.magnitude = magnitude;
//    }

//    public PrimitiveIterator.OfInt getPositions() {
//        return Arrays.stream(positions).iterator();
//    }
//
//    public IntStream getPositionStream() {
//        return Arrays.stream(positions);
//    }
//
//    public int getDimensionNumber() {
//        return positions.length;
//    }
//
//    /**
//     *
//     * @param termPosition
//     * @return
//     */
//    public float getValue(int termPosition) {
//        return values[termPosition];
//    }
//
}
