/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance;

import java.util.Map;

/**
 * @author david
 */
public class CosDistanceSparse implements DistanceFunc<Map> {

    public static final String POSITIONS_FIELD = "_positions";

    public static final String VALUES_FIELD = "_values";

    public static final String MAGNITUDE_FIELD = "_magnitude";

    @Override
    public float getDistance(Map o1, Map o2, float threshold) {
        int [] positions = (int []) o1.get(POSITIONS_FIELD);
        float [] values = (float []) o1.get(VALUES_FIELD);

        int [] positions2 = (int []) o2.get(POSITIONS_FIELD);
        float [] values2 = (float []) o2.get(VALUES_FIELD);

        // Initialize computing variables
        double dotProduct = 0;
        int i1 = 0;
        int i2 = 0;
        do {
            if (positions[i1] > positions2[i2]) {
                i2++;
            } else if (positions[i1] < positions2[i2]) {
                i1++;
            } else {
                dotProduct += values[i1++] * values2[i2++];
            }
        } while (i1 < positions.length && i2 < positions2.length);

        double distance = 1.0 - (dotProduct / ((float) o1.get(MAGNITUDE_FIELD) * (float) o2.get(MAGNITUDE_FIELD) ));
        // Compensate for float sizing error
        if (distance < 0.0000001)
            return 0;
        return (float) distance;    }

    @Override
    public Class<Map> getObjectClass() {
        return Map.class;
    }


//
//    public CosDistanceSparse(BufferedReader stream) throws EOFException, IOException, NumberFormatException {
//        super(stream);
//        magnitude = calculateMagnitude();
//    }
//
//    public CosDistanceSparse(TIntFloatMap map, boolean normalize) throws EOFException, IOException, NumberFormatException {
//        super(map, normalize);
//        magnitude = normalize ? 1f : calculateMagnitude();
//    }
//
//    public CosDistanceSparse(ObjectSparseVector copyFrom, boolean normalize) throws EOFException, IOException, NumberFormatException {
//        super(copyFrom, normalize);
//        magnitude = normalize ? 1f : calculateMagnitude();
//    }
//
//    protected CosDistanceSparse(String locator, int[] positions, float[] values, float magnitude) {
//        super(locator, positions, values);
//        this.magnitude = magnitude;
//    }

//    public PrimitiveIterator.OfInt getPositions() {
//        return Arrays.stream(positions).iterator();
//    }
//
//    public IntStream getPositionStream() {
//        return Arrays.stream(positions);
//    }
//
//    public int getDimensionNumber() {
//        return positions.length;
//    }
//
//    /**
//     *
//     * @param termPosition
//     * @return
//     */
//    public float getValue(int termPosition) {
//        return values[termPosition];
//    }
//
}
