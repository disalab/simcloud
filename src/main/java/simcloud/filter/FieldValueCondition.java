/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.filter;

import messif.record.Record;
import messif.record.RecordImpl;

import java.util.Arrays;
import java.util.Collections;

import static simcloud.filter.SelectionCondition.createException;

/**
 * A root interface for filtering JSON maps (meta objects) according to various types of conditions.
 * @author david
 */
public class FieldValueCondition {
    /**
     *
     * @param queryJSON
     * @return
     */
    public static SelectionCondition valueOf(final Object queryJSON) throws IllegalArgumentException {
        if (queryJSON instanceof Record) {
            if (((Record) queryJSON).getFieldCount() != 1) throw createException(queryJSON);
            String key = ((Record) queryJSON).getFieldNames().iterator().next();
            return valueOperator(key, ((Record) queryJSON).getField(key));
        }
        if (queryJSON instanceof Boolean) {
            return new SelectionCondition<Boolean>() {
                @Override
                public boolean matches(Boolean object) {
                    return queryJSON.equals(object);
                }
                @Override
                public Class<Boolean> getExpectedClass() {
                    return Boolean.class;
                }
            };
        }
        if (queryJSON instanceof String) {
            // this condition can be evaluated either on a field of type String or on String []
            return new SelectionCondition<Object>() {
                @Override
                public boolean matches(Object object) {
                    if (object instanceof String)
                        return queryJSON.equals(object);
                    if (object instanceof String []) for (String v : (String[]) object)
                        if (queryJSON.equals(v)) return true;
                    return false;
                }
                @Override
                public Class<Object> getExpectedClass() {
                    return Object.class;
                }
            };
        }
        // this condition can be evaluated either on a field of type Integer or on int []
        if (queryJSON instanceof Integer) return new SelectionCondition<Object>() {
            @Override
            public boolean matches(Object object) {
                if (object instanceof Integer)
                    return queryJSON.equals(object);
                if (object instanceof int[]) for (int v : (int[]) object)
                    if (queryJSON.equals(v)) return true;
                return false;
            }

            @Override
            public Class<Object> getExpectedClass() {
                return Object.class;
            }
        };
        // this condition can be evaluated either on a field of type Float or on float []
        if (queryJSON instanceof Float) return new SelectionCondition<Object>() {
            @Override
            public boolean matches(Object object) {
                if (object instanceof Float)
                    return queryJSON.equals(object);
                if (object instanceof float[]) for (float v : (float[]) object)
                    if (queryJSON.equals(v)) return true;
                return false;
            }

            @Override
            public Class<Object> getExpectedClass() {
                return Object.class;
            }
        };
        if (queryJSON instanceof float []) {
            return new SelectionCondition<float []>() {
                @Override
                public boolean matches(float [] object) {
                    return Arrays.equals(object, (float []) queryJSON);
                }
                @Override
                public Class<float []> getExpectedClass() {
                    return float [].class;
                }
            };
        }
        if (queryJSON instanceof int []) {
            return new SelectionCondition<int []>() {
                @Override
                public boolean matches(int [] object) {
                    return Arrays.equals(object, (int []) queryJSON);
                }
                @Override
                public Class<int []> getExpectedClass() {
                    return int [].class;
                }
            };
        }
        if (queryJSON instanceof String []) {
            return new SelectionCondition<String []>() {
                @Override
                public boolean matches(String [] object) {
                    return Arrays.equals(object, (String []) queryJSON);
                }
                @Override
                public Class<String []> getExpectedClass() {
                    return String [].class;
                }
            };
        }
        if (queryJSON == null) {
            return new SelectionCondition<Object>() {
                @Override
                public boolean matches(Object object) {
                    return object == null;
                }
                @Override
                public Class<Object> getExpectedClass() {
                    return Object.class;
                }
            };
        }

        throw createException(queryJSON);
    }

    //public static SelectionCondition valueOperator(final Map.Entry<String, Object> queryJSON) throws IllegalArgumentException {
    public static SelectionCondition valueOperator(String key, Object value) throws IllegalArgumentException {
        // equality
        if (SelectionCondition.Operator.EQ.equals(key)) {
            return valueOf(value);
        }
        // not equals
        if (SelectionCondition.Operator.NE.equals(key)) {
            final SelectionCondition condition = valueOf(value);
            return new SelectionCondition() {
                @Override
                public boolean matches(Object object) {
                    if (object == null || ! condition.getExpectedClass().isAssignableFrom(object.getClass())) {
                        return ! condition.matches(null);
                    }
                    return ! condition.matches(condition.getExpectedClass().cast(object));
                }

                @Override
                public Class getExpectedClass() {
                    return condition.getExpectedClass();
                }
            };
        }
        // less than, greater than,...
        if (SelectionCondition.Operator.GT.equals(key)
                || SelectionCondition.Operator.GTE.equals(key)
                || SelectionCondition.Operator.LT.equals(key)
                || SelectionCondition.Operator.LTE.equals(key)) {

            if (! (value instanceof Number)) {
                throw createException(Collections.singletonMap(key, value));
            }
            final double doubleValue = ((Number) value).doubleValue();

            return new SelectionCondition<Number>() {
                @Override
                public boolean matches(Number object) {
                    if (object == null) {
                        return false;
                    }
                    switch (key) {
                        case SelectionCondition.Operator.GT:
                            return object.doubleValue() > doubleValue;
                        case SelectionCondition.Operator.GTE:
                            return object.doubleValue() >= doubleValue;
                        case SelectionCondition.Operator.LT:
                            return object.doubleValue() < doubleValue;
                        case SelectionCondition.Operator.LTE:
                            return object.doubleValue() <= doubleValue;
                    }
                    return false;
                }
                @Override
                public Class getExpectedClass() {
                    return Number.class;
                }
            };
        }

        // $in [ list of values ]
        if (SelectionCondition.Operator.IN.equals(key)) {
            return inOperator(value);
        }

        if (SelectionCondition.Operator.NIN.equals(key)) {
            final SelectionCondition conditionToNegate = inOperator(value);
            return new SelectionCondition() {
                @Override
                public boolean matches(Object object) {
                    return ! conditionToNegate.matches(object);
                }
                @Override
                public Class getExpectedClass() {
                    return conditionToNegate.getExpectedClass();
                }
            };
        }

        // else, this must an embedded document (another level of JSON)
        return MapCondition.valueOf(new RecordImpl(Collections.singletonMap(key, value), true));
    }

    /**
     * Implementation of the operator:   { "$in": [ 3, 4, 5 ] }
     * @param arrayValue the array part of the condition after "$in":
     * @return a crated condition that checks that given value is in the condition array
     */
    protected static SelectionCondition inOperator(Object arrayValue) {
        if (arrayValue instanceof float []) {
            return new SelectionCondition<Float>() {
                @Override
                public boolean matches(Float object) {
                    if (object == null) return false;
                    for (float v : (float []) arrayValue)
                        if (v == object) return true;
                    return false;
                }
                @Override
                public Class<Float> getExpectedClass() {
                    return Float.class;
                }
            };
        }
        if (arrayValue instanceof int []) {
            return new SelectionCondition<Integer>() {
                @Override
                public boolean matches(Integer object) {
                    if (object == null) return false;
                    for (int v : (int[]) arrayValue)
                        if (v == object) return true;
                    return false;
                }
                @Override
                public Class<Integer> getExpectedClass() {
                    return Integer.class;
                }
            };
        }
        if (arrayValue instanceof String []) return new SelectionCondition<String>() {
            @Override
            public boolean matches(String object) {
                if (object == null) return false;
                for (String v : (String[]) arrayValue)
                    if (object.equals(v)) return true;
                return false;
            }
            @Override
            public Class<String> getExpectedClass() {
                return String.class;
            }
        };
        throw createException(arrayValue);
    }

}
