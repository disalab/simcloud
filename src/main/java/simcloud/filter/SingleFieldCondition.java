/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.filter;


import messif.record.Record;

import java.util.Collections;
import java.util.Map;

/**
 * Representation of a {@link SelectionCondition} to match a value of given field.
 * @author david
 * @param <T> type of the value to be checked in the field
 */
public class SingleFieldCondition<T> extends MapCondition {
    
    /** ID for Java serialization. */
    private static final long serialVersionUID = 345601L;
    
    /** Name of the field on which to check the condition. */
    protected final String fieldName;

    /** The specific sub-object condition to check. */
    protected final SelectionCondition<T> condition;

    /**
     * Creates new field condition on given field name and sub-object condition.
     * @param fieldName Name of the field to check the condition
     * @param condition specific sub-object condition to check
     */
    public SingleFieldCondition(String fieldName, SelectionCondition condition) {
        this.fieldName = fieldName;
        this.condition = condition;
    }
    
    @Override
    public boolean matches(Map record) {
        if (record == null) {
            return false;
        }
        Object object = record.get(fieldName);
        if (object == null || ! condition.getExpectedClass().isAssignableFrom(object.getClass())) {
            return condition.matches(null);
        }
        return condition.matches(condition.getExpectedClass().cast(object));
    }

    //public static SelectionCondition<Map> valueOf(Map.Entry<String, Object> queryJSON) {
    public static SelectionCondition<Map> valueOf(String key, Object value) {
        if (Operator.AND.equals(key) || Operator.OR.equals(key)) {
            return AggregationCondition.valueOf(value, Operator.AND.equals(key));
        }
        if (Operator.NOT.equals(key)) {
            if (!(value instanceof Record)) {
                throw SelectionCondition.createException(Collections.singletonMap(key, value));
            }
            final SelectionCondition<Map> conditionToNegate = MapCondition.valueOf((Record) value);
            return new MapCondition() {
                @Override
                public boolean matches(Map object) {
                    return ! conditionToNegate.matches(object);
                }
            };
        }
        return new SingleFieldCondition<>(key, FieldValueCondition.valueOf(value));
    }

}
