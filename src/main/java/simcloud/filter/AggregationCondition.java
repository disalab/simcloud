/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.filter;


import messif.record.Record;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Condition on data object (meta-object, field map) which is a conjuction several conditions.
 * @author david
 */
public class AggregationCondition extends MapCondition {

    /** ID for Java serialization. */
    private static final long serialVersionUID = 34415602L;

    /** Conjunction of these conditions must be satisfied, if the object is to match the condition represented by this object. */
    protected final List<SelectionCondition<Map>> subConditions;
    private final boolean isAnd;

    /**
     * Creates a new condition as a conjuction of the passed conditions.
     * @param subConditions sub-conditions to be fulfilled
     */
    public AggregationCondition(List<SelectionCondition<Map>> subConditions, boolean isAnd) {
        this.subConditions = Collections.unmodifiableList(subConditions);
        this.isAnd = isAnd;
    }
    
    @Override
    public boolean matches(Map metaObject) {
        for (SelectionCondition<Map> subCondition : subConditions) {
            if (subCondition.matches(metaObject)) {
                if (! isAnd) {
                    return true;
                }
            } else {
                if (isAnd) {
                    return false;
                }
            }
        }
        return isAnd;
    }


    public static SelectionCondition<Map> valueOf(Object queryJSON, boolean isAnd) {
        if (! (queryJSON instanceof Object [])) {
            throw SelectionCondition.createException(queryJSON);
        }
        Object [] conditionArray = (Object []) queryJSON;
        if (conditionArray.length == 0) {
            return isAnd ? MapCondition.TRUE_CONDITION : MapCondition.FALSE_CONDITION;
        }
        List<Record> subConditions = new ArrayList<>(conditionArray.length);
        for (Object condition : conditionArray) {
            if (! (condition instanceof Record)) {
                throw SelectionCondition.createException(queryJSON);
            }
            subConditions.add((Record) condition);
        }
        final List<SelectionCondition<Map>> collect = new ArrayList<>(subConditions.size());
        subConditions.stream().map((Function<Record, SelectionCondition>) MapCondition::valueOf).forEach(collect::add);
        return  new AggregationCondition(collect, isAnd);
    }

    /**
     * Standard Mongo-like and: { status: "A", age: { $lt: 30 } }
     * @param queryJSONs
     * @return
     */
    public static AggregationCondition valueOf(final Record queryJSONs) {
        return new AggregationCondition(queryJSONs.getFieldNames().stream().sequential()
                .map(key -> SingleFieldCondition.valueOf(key, queryJSONs.getField(key)))
                .collect(Collectors.toList()), true);
//        return new AggregationCondition(queryJSONs.entrySet().stream()
//                .map(entry -> SingleFieldCondition.valueOf(entry.getKey(), entry.getValue()))
//                .collect(Collectors.toList()), true);
    }


}
