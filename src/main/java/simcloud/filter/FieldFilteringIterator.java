/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.filter;


import messif.data.DataObject;
import messif.data.util.DataObjectIterator;

import java.util.NoSuchElementException;

/**
 * Encapsulating iterator that filters out all objects that do not match a condition specified
 *  by {@link SelectionCondition}.
 * @author david
 */
public class FieldFilteringIterator extends DataObjectIterator {

    protected final DataObjectIterator encapsulatedIterator;
    
    protected final SelectionCondition<DataObject> condition;

    protected DataObject nextObject = null;
    
    public FieldFilteringIterator(DataObjectIterator encapsulatedIterator, SelectionCondition<DataObject> filter) {
        this.encapsulatedIterator = encapsulatedIterator;
        this.condition = filter;
    }
    
    @Override
    public DataObject getCurrentObject() throws NoSuchElementException {
        return encapsulatedIterator.getCurrentObject();
    }

    @Override
    public boolean hasNext() {
        if (nextObject != null) {
            return true;
        }
        return setNextMatchingObject();
    }

    @Override
    public DataObject next() {
        if (nextObject == null && ! setNextMatchingObject()) {
            throw new NoSuchElementException();
        }
        try {
            return nextObject;
        } finally {
            nextObject = null;
        }
    }
    
    private boolean setNextMatchingObject() {
        while (encapsulatedIterator.hasNext()) {
            DataObject next = encapsulatedIterator.next();
            if (condition.matches(next)) {
                nextObject = next;
                return true;
            }
        }
        return false;
    }

    @Override
    public void remove() {
        encapsulatedIterator.remove();
    }
    
}
