/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.filter;


import messif.record.Record;

import java.util.Map;

/**
 * Condition on data object (field map) which is a conjunction several conditions.
 * @author david
 */
public abstract class MapCondition implements SelectionCondition<Map> {

    /** ID for Java serialization. */
    private static final long serialVersionUID = 3425602L;

    @Override
    public Class<Map> getExpectedClass() {
        return Map.class;
    }

    public static SelectionCondition<Map> valueOf(Record queryJSON) {
        if (queryJSON.getFieldCount() > 1) {
            return AggregationCondition.valueOf(queryJSON);
        }
        if (queryJSON.getFieldCount() == 0) {
            return new MapCondition() {
                @Override
                public boolean matches(Map object) {
                    return true;
                }
            };
        }
        String theOnlyKey = queryJSON.getFieldNames().iterator().next();
        return SingleFieldCondition.valueOf(theOnlyKey, queryJSON.getField(theOnlyKey));
                //entrySet().iterator().next());
    }


    protected static SelectionCondition<Map> TRUE_CONDITION = new MapCondition() {
        @Override
        public boolean matches(Map object) {
            return true;
        }
    };

    protected static SelectionCondition<Map> FALSE_CONDITION = new MapCondition() {
        @Override
        public boolean matches(Map object) {
            return false;
        }
    };
}
