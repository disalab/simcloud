/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.filter;

import messif.record.Record;
import messif.utility.json.JSONWriter;

import java.io.Serializable;

/**
 * A root interface for filtering JSON maps according to selection conditions
 * @author david
 * @param <T> expected type of the field (sub-object) on which this condition is to be processed
 */
public interface SelectionCondition<T> extends Serializable {

    class Operator {
        public static final String AND = "$and";
        public static final String OR = "$or";
        public static final String NOT = "$not";
        public static final String EQ = "$eq";
        public static final String NE = "$ne";

        public static final String GT = "$gt";
        public static final String GTE = "$gte";
        public static final String LT = "$lt";
        public static final String LTE = "$lte";

        public static final String IN = "$in";
        public static final String NIN = "$nin";
    }

    /** 
     * Name of the parameter used within {@link Record#getField} that is
     *  to carry the condition.
     */
    //String paramPrefix = "FILTER_";
    
    /** 
     * Checks whether given object matches given condition.
     * @param object
     * @return true if given object matches given condition
     */
    boolean matches(T object);
    
    /**
     * Returns the specific class expected by this condition: {@code T}.
     * @return class {@link T}
     */
    Class<T> getExpectedClass();


//    /**
//     *
//     * @param queryJSON
//     * @return
//     */
//    static SelectionCondition valueOf(final Object queryJSON) throws IllegalArgumentException {
//
//    }

    static IllegalArgumentException createException(Object queryJSON) {
        return new IllegalArgumentException("cannot parse this Mongo-like condition: " + JSONWriter.writeToJSON(queryJSON, false));
    }
}
