/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.filter;


import messif.data.DataObject;
import messif.record.Record;

import java.util.Map;

/**
 * Just a wrapper that encapsulates a condition on the Map in the {@link DataObject}.
 * @author david
 */
public class DataObjectCondition implements SelectionCondition<DataObject> {

    /** ID for Java serialization. */
    private static final long serialVersionUID = 3425602L;

    /**
     * Name of the operation field to contain a filter.
     */
    public static final String QUERY_FILTER_PARAM = "filter";

    protected final SelectionCondition<Map> mapCondition;

    public DataObjectCondition(SelectionCondition<Map> mapCondition) {
        this.mapCondition = mapCondition;
    }

    @Override
    public boolean matches(DataObject object) {
        return mapCondition.matches(object.getMap());
    }

    @Override
    public Class<DataObject> getExpectedClass() {
        return DataObject.class;
    }

    /**
     * Creates a condition to be applied on data object given a list of parameters.
     * @return null or created condition on data object is created
     */
    public static SelectionCondition<DataObject> valueOf(Record queryJSON) throws IllegalArgumentException {
        return new DataObjectCondition(MapCondition.valueOf(queryJSON));
    }

}
