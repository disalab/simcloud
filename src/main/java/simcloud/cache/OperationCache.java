/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.cache;

import messif.algorithm.Algorithm;
import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.OneStepProcessor;
import messif.utility.ExtendedProperties;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * @author david
 */
public abstract class OperationCache<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer> extends Algorithm {

    public static final long DEFAULT_MAX_TIME_MS = 300000;
    public static final int DEFAULT_MAX_ENTRIES = 1000;

    public static final String ANSWER_PARAM = "answer";

    protected final Class<TRequest> operationClass;
    protected final Class<TAnswer> answerClass;
    //protected final int argumentIndex;
    protected final Predicate<TRequest> filter;
    protected final Function<TRequest, String> paramGetter;
    protected long maxTime = DEFAULT_MAX_TIME_MS;
    protected int maxEntries = DEFAULT_MAX_ENTRIES;

    public OperationCache(String name, Class<TRequest> operationClass, Class<TAnswer> answerClass) throws IllegalArgumentException {
        this(name, operationClass, answerClass, (operation) -> true, (operation) -> operation.toString(), DEFAULT_MAX_TIME_MS, DEFAULT_MAX_ENTRIES);
    }

    public OperationCache(String name, Class<TRequest> operationClass, Class<TAnswer> answerClass, Predicate<TRequest> filter, Function<TRequest, String> paramGetter, long maxTime, int maxEntries) throws IllegalArgumentException {
        super(name, defaultConfiguration());
        this.operationClass = operationClass;
        this.answerClass = answerClass;
        this.filter = filter;
        this.paramGetter = paramGetter;
        this.maxEntries = maxEntries;
        this.maxTime = maxTime;
    }

    protected static ExtendedProperties defaultConfiguration() {
        final ExtendedProperties defaultConfig = new ExtendedProperties();
        defaultConfig.put(CONFIG_EXECUTOR_PREFIX + "1", "messif.algorithm.executor.OperationLogger");
        return defaultConfig;
    }

    protected void registerProcessors() {
        super.registerProcessors();
        this.handler.register(ProcessorConfiguration.create(operationClass, AbstractAnswer.class)
                .withProcessor(operation -> operation.getParams().containsField(ANSWER_PARAM)
                        ? new AddToCacheProcessor(operation)
                        : new UseCacheProcessor(operation)).executeSequential().build());
    }

    abstract TAnswer getCachedAnswer(String operationString);

    abstract TAnswer putIfAbsent(String operationString, TAnswer answer);

    /**
     * Processor that tries to find the operation in the cache.
     */
    protected class UseCacheProcessor extends OneStepProcessor<TRequest, TAnswer> {

        public UseCacheProcessor(TRequest operation) {
            super(operation);
//            this.cachedOperation = cachedOperation;
        }

        private TAnswer cachedAnswer;

        @Override
        protected void process() throws AlgorithmMethodException {
            if ((this.cachedAnswer = getCachedAnswer(paramGetter.apply(operation))) != null) {
                // Logger.getLogger(OperationCache.class.getName()).info("HIT Ispn cache for operation: " + operation);
            }
        }

        /**
         * Returns the cache answer or NULL if the operation is not cached.
         * @return
         */
        @Override
        public TAnswer finish() {
            return cachedAnswer;
            // return cachedAnswer != null ? cachedAnswer : AbstractAnswer.createAnswer(ProcessingStatus.UNKNOWN_ERROR, answerClass);
        }
    }

    /**
     * Processor that stores the successfully processed operation answer into the cache.
     */
    protected class AddToCacheProcessor extends OneStepProcessor<TRequest, AbstractAnswer> {

        public AddToCacheProcessor(TRequest operation) {
            super(operation);
        }

        @Override
        protected void process() throws AlgorithmMethodException {
            if (! filter.test(operation)) {
                return;
            }
            final TAnswer answer = operation.getParams().getField(ANSWER_PARAM, answerClass);
            if (answer.wasSuccessful() && putIfAbsent(paramGetter.apply(operation), answer) == null) {
                // Logger.getLogger(OperationCache.class.getName()).info("STORED into Ispn cache operation: " + operation);
            }
        }

        @Override
        public TAnswer finish() {
            return operation.getParams().getField(ANSWER_PARAM, answerClass);
        }
    }
}
