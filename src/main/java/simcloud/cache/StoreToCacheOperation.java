/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.cache;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

/**
 * Created by david on 13/12/16.
 */
@Deprecated
public interface StoreToCacheOperation extends AbstractOperation {

    String TYPE_STRING = "storeToCache";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }


    String OPERATION_PARAM = "operation";

    @Get(field = OPERATION_PARAM)
    @Required
    AbstractOperation getOperation();


    String ANSWER_PARAM = "answer";

    @Get(field = ANSWER_PARAM )
    @Required
    AbstractAnswer getAnswer();

}
