/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simcloud.cache;

import messif.operation.AbstractOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.RankedAnswer;
import messif.operation.search.KNNOperation;

/**
 *
 * @author david
 */
public class DummyOperationCache<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer> extends OperationCache<TRequest, TAnswer> {

    public DummyOperationCache(Class<TRequest> operationClass, Class<TAnswer> answerClass) throws IllegalArgumentException {
        super("Dummy operation cache that does not cache anything", operationClass, answerClass);
    }

    @Override
    TAnswer getCachedAnswer(String operationString) {
        return null;
    }

    @Override
    TAnswer putIfAbsent(String operationString, TAnswer answer) {
        return null;
    }


    /**
     * Create a dummy cache that does nothing.
     */
    public static DummyOperationCache<KNNOperation, RankedAnswer> kNNOperationCache() {
        // create the cache
        return new DummyOperationCache<>(KNNOperation.class, RankedAnswer.class);
    }

}
