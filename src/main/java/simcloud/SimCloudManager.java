/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud;


import messif.algorithm.Algorithm;
import messif.operation.processing.OperationEvaluator;
import messif.operation.processing.ProcessorConfiguration;
import messif.utility.ExtendedProperties;
import simcloud.config.ProcessorConfig;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

/**
 * Wrapper for any number of algorithms - indexes, key-object storages etc.
 * Processing of operations on this manager is controlled by a list of {@link simcloud.config.ProcessorConfig} objects.
 * Given an operation, the first matcher that can process this operation processes this operation.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class SimCloudManager extends Algorithm {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 8592104L;
    
    /** Log to be used by this package. */
    public final static Logger logger = Logger.getLogger(SimCloudManager.class.getName());

    /** Name of this manager as algorithm that can be used in the configurable processors. */
    public static final String MANAGER_ALG_NAME = "manager";

    /** List of all algorithms and indexes covered by this manager with their names. */
    protected final Map<String, OperationEvaluator> algorithms;
    
    /** List of operation matchers that define which operations will be processed in which way. */
    protected final List<ProcessorConfig> processorConfigs;

    /**
     * Creates a new Similarity Cloud manager for given algorithms and operation matchers.
     *
     * @param indexAlgorithms list of algorithm names
     * @param algorithmNames list of algorithms (in the same order as names in the previous argument)
     * @param executors variable number of operation matchers to control processing of operations on this manager
     * @throws IllegalArgumentException if arrays of algorithms and their names do not match
     */
    @AlgorithmConstructor(description = "creates locator-storage wrapper for the given algorithm", arguments = {"map of 'name': 'algorithm'"})
    public SimCloudManager(OperationEvaluator [] indexAlgorithms, String [] algorithmNames, ProcessorConfig ... executors) throws IllegalArgumentException {
        this(indexAlgorithms, algorithmNames, Algorithm.defaultConfiguration(), executors);
    }

    /**
     * Creates a new Similarity Cloud manager for given algorithms and operation matchers.
     * 
     * @param indexAlgorithms list of algorithm names
     * @param algorithmNames list of algorithms (in the same order as names in the previous argument)
     * @param executors variable number of operation matchers to control processing of operations on this manager
     * @throws IllegalArgumentException if arrays of algorithms and their names do not match
     */
    @AlgorithmConstructor(description = "creates locator-storage wrapper for the given algorithm", arguments = {"map of 'name': 'algorithm'"})
    public SimCloudManager(OperationEvaluator [] indexAlgorithms, String [] algorithmNames, ExtendedProperties algConfig, ProcessorConfig ... executors) throws IllegalArgumentException {
        super("Similarity Manager", algConfig);
        this.algorithms = new HashMap<>();
        if (indexAlgorithms == null || algorithmNames == null || indexAlgorithms.length != algorithmNames.length) {
            throw new IllegalArgumentException("arrays with algorithms and with their names must be of the same length");
        }
        for (int i = 0; i < indexAlgorithms.length; i++) {
            algorithms.put(algorithmNames[i], indexAlgorithms[i]);            
        }
        // the manager is a registered algorithm itself
        algorithms.put(MANAGER_ALG_NAME, this);
        processorConfigs = new ArrayList<>(Arrays.asList(executors));
    }

    @Override
    public synchronized boolean init() {
        for (OperationEvaluator evaluator : algorithms.values()) {
            if ((evaluator instanceof Algorithm) && (evaluator != this))
                ((Algorithm) evaluator).init();
        }
        return super.init();
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();
        for (ProcessorConfig processorConfig : processorConfigs) {
            registerProcessor(processorConfig);
        }
    }

    protected void registerProcessor(ProcessorConfig processorConfig) {
        processorConfig.setManager(this);
        this.handler.register(processorConfig);
    }

    /**
     * Ads the given executor to the end of the list of executors.
     * @param processorConfiguration new executor to be added
     */
    public void addExecutor(ProcessorConfig processorConfiguration) {
        processorConfigs.add(processorConfiguration);
        if (isInitialized) {
            registerProcessor(processorConfiguration);
        }
    }

    /**
     * Get a formatted string with the numbered list of executors.
     * @return string where on each line is output of {@link ProcessorConfiguration#toString() }
     */
    public String listExecutors() {
        StringBuilder stringBuilder = new StringBuilder();
        int counter = 0;
        for (ProcessorConfiguration processorConfiguration : processorConfigs) {
            stringBuilder.append(counter++).append(". ").append(processorConfiguration.toString()).append('\n');
        }
        return stringBuilder.toString();
    }

    
//    @Override
//    public void setOperationsThreadPool(ExecutorService operationsThreadPool) {
//        super.setOperationsThreadPool(operationsThreadPool);
//        for (Algorithm algorithm : allAlgorithms.values()) {
//            algorithm.setOperationsThreadPool(operationsThreadPool);
//        }
//    }

    /**
     * Returns map of algorithms and their names covered by this SimCloud manager.
     * @return map of name-algorithm
     */
    public Map<String, OperationEvaluator> getAlgorithms() {
        return Collections.unmodifiableMap(algorithms);
    }

    public Map<String, OperationEvaluator> getNamedAlgorithms() {
        return Collections.unmodifiableMap(algorithms);
    }

    /**
     * Returns a formatted string with the algorithms in this manager and their names.
     * @return formatted string with the algorithms in this manager and their names
     */
    public String listAlgorithms() {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, OperationEvaluator> algorithm : algorithms.entrySet()) {
            final OperationEvaluator evaluator = algorithm.getValue();
            String algString = (evaluator instanceof Algorithm) ? (((Algorithm) evaluator).getName()) : evaluator.toString();
            builder.append('\t').append(algorithm.getKey()).append(" = ").append(algString).append('\n');
        }
        builder.delete(builder.length() - 2, builder.length());
        return builder.toString();
    }
    
    /**
     * Ads a specified algorithm under specified name
     * @param algName name of the added algorithm
     * @param algorithm algorithm to add
     */
    public void addAlgorithm(String algName, Algorithm algorithm) {
        // super.addAlgorithm(algorithm);
        algorithms.put(algName, algorithm);
        for (ProcessorConfiguration processorConfig : processorConfigs) {
            if (processorConfig instanceof ProcessorConfig) {
                ((ProcessorConfig) processorConfig).setManager(this);
            }
        }
    }

    /**
     * Thread pool service to process operations in threads.
     */
    public ExecutorService getThreadPool() {
        return threadPool;
    }

    @Override
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    public void finalize() throws Throwable {
        for (OperationEvaluator algorithm : algorithms.values()) {
            if (algorithm instanceof Algorithm) {
                ((Algorithm) algorithm).finalize();
            }
        }
        super.finalize();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(getName()).append(" with the following algorithms:\n");
        builder.append(listAlgorithms());
        // builder.append("\nOperation executors :\n");
        // builder.append(listExecutors());
        return builder.toString();
    }
}
