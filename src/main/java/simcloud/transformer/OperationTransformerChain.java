/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer;


import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;

/**
 * 
 * @author xnovak8
 * @param <O> type of the original operation
 * @param <T> type of the next step operation
 * @param <A> type of the final answer to be returned from the whole processing
 */
public interface OperationTransformerChain<O extends AbstractOperation, T extends AbstractOperation, A extends AbstractAnswer>
        extends OperationTransformer<O, T> {
    /**
     * Updates the original operation from a processed operation of other type.
     * @param origOperation original operation that entered the processing chain
     * @param returnedAnswer answer from the processed transformed operation
     */
    O updateOrigOperation(O origOperation, T processedOperation, AbstractAnswer returnedAnswer);

    /**
     * Produces the final answer given the oririginal operation, the transformed operation and its answer.
     * @param origOperation original operation that entered the processing chain
     * @param processedOperation the transformed operation transformed
     * @param returnedAnswer answer from the processed transformed operation
     * @return the final answer of the whole processing (do not continue in any potential further steps or NULL (if the
     *    returned answer was not {@link AbstractAnswer#wasSuccessful() successful}.
     */
    A updateAnswer(O origOperation, T processedOperation, AbstractAnswer returnedAnswer);

}
