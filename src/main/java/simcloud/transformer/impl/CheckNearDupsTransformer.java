/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.operation.OperationBuilder;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ListingAnswer;
import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.crud.InsertOperation;
import messif.operation.params.ApproximateSearch;
import messif.operation.params.DataObjects;
import messif.operation.params.DistanceFunction;
import messif.operation.params.Range;
import messif.operation.search.CheckNearDupsOperation;
import simcloud.transformer.OperationTransformerChain;

import java.util.HashMap;
import java.util.Map;

import static messif.operation.params.ApproximateSearch.Type.ABS_OBJ_COUNT;

/**
 * Given an insert operation, this step checks that the objects don't have near-duplicates in the index. Those
 *  objects that do have near-dups, are removed from the insert operation and put into the "skipped_objects" field of
 *  the answer.
 * @author david
 */
public class CheckNearDupsTransformer implements OperationTransformerChain<InsertOperation, CheckNearDupsOperation, ListingAnswer> {

    final protected Map<String, Object> additionalParams;
    private final float defaultRadius;

    public CheckNearDupsTransformer(float defaultRadius) {
        this.defaultRadius = defaultRadius;
        additionalParams = new HashMap<>();
        additionalParams.put(ApproximateSearch.APPROX_TYPE_FIELD, ABS_OBJ_COUNT);
        additionalParams.put(ApproximateSearch.APPROX_PARAM_FIELD, 1);
        additionalParams.put("PERCENTILE", 0.1f);
    }

    @Override
    public CheckNearDupsOperation transformOperation(InsertOperation origOperation) {
        final OperationBuilder<CheckNearDupsOperation> builder = OperationBuilder.create(CheckNearDupsOperation.class);
        for (Map.Entry<String, Object> param : additionalParams.entrySet()) {
            builder.addParam(param.getKey(), param.getValue());
        }
        builder.addParam(DataObjects.OBJECTS_FIELD, origOperation.getObjects());
        builder.addParam(Range.RADIUS_FIELD, origOperation.getParams().getField(Range.RADIUS_FIELD, Float.TYPE, defaultRadius));
        if (origOperation.getParams().containsField(DistanceFunction.DISTANCE_FIELD)) {
            builder.addParam(DistanceFunction.DISTANCE_FIELD, origOperation.getParams().getField(DistanceFunction.DISTANCE_FIELD));
        }
        return builder.build();
    }

    @Override
    public InsertOperation updateOrigOperation(InsertOperation origOperation, CheckNearDupsOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (!(returnedAnswer instanceof ProcessObjectsAnswer)) {
            return origOperation;
        }
        // now replace the objects to be inserted with the result of this pre-processing step
        // the wrong (skipped) objects are not to be inserted
        origOperation.getParams().setField(DataObjects.OBJECTS_FIELD, ((ProcessObjectsAnswer) returnedAnswer).getAnswerObjects());
        return origOperation;
    }

    @Override
    public ListingAnswer updateAnswer(InsertOperation origOperation, CheckNearDupsOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (returnedAnswer instanceof ProcessObjectsAnswer && (((ProcessObjectsAnswer) returnedAnswer).getSkippedObjects() != null)) {
            // return an answer that contains just the errors (skipped objects)
            return ProcessObjectsAnswer.createFailed(returnedAnswer.getStatus(), ((ProcessObjectsAnswer) returnedAnswer).getSkippedObjects());
        }
        return null;
    }
}
