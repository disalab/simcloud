/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.operation.ReturnDataOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ListingAnswer;
import simcloud.transformer.OperationTransformerChain;

/**
 * This transformer is used, e.g., for the {@link messif.operation.search.GetRandomObjectsOperation}: after the
 *  operation is processed on the PPP-Codes index, the answer from the PPP-Codes is stored in the "id_list" field
 *  of the original operation. Subsequently, the {@link GetObjectForIDsTransformer} is used.
 * @author david
 */
public class IdsToOperation implements OperationTransformerChain<ReturnDataOperation, ReturnDataOperation, AbstractAnswer> { // OperationTransformer<LocatorQueryOperation, AbstractOperation, GetObjectsByLocatorsOperation> {

    /** Temporary field used to exchange the object IDs between two steps of random-object operation processing. */
    public static final String ID_LIST_FIELD = "id_list";

    @Override
    public ReturnDataOperation transformOperation(ReturnDataOperation origOperation) {
        return origOperation;
    }

    @Override
    public ReturnDataOperation updateOrigOperation(ReturnDataOperation origOperation, ReturnDataOperation processedOperation, AbstractAnswer returnedAnswer) {
        if ((returnedAnswer instanceof ListingAnswer) && (returnedAnswer.wasSuccessful()) && (((ListingAnswer) returnedAnswer).getAnswerCount() > 0)) {
            origOperation.getParams().setField(ID_LIST_FIELD, ((ListingAnswer) returnedAnswer).getAnswerObjects());
        }
        return origOperation;
    }

    @Override
    public AbstractAnswer updateAnswer(ReturnDataOperation origOperation, ReturnDataOperation processedOperation, AbstractAnswer returnedAnswer) {
        if ((returnedAnswer instanceof ListingAnswer) && (returnedAnswer.wasSuccessful()) && (((ListingAnswer) returnedAnswer).getAnswerCount() > 0)) {
            return null;
        }
        return returnedAnswer;
    }
}
