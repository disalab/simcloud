/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.operation.ProcessObjectsOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.params.QueryObject;
import messif.operation.search.QueryObjectOperation;
import messif.utility.ProcessingStatus;
import simcloud.transformer.OperationTransformerChain;

/**
 * Given a query operation, the queyr object is processed by a given processor and replaced in the query operation.
 * @author david
 */
public class PreprocessQueryTransformer
        implements OperationTransformerChain<QueryObjectOperation, ProcessObjectsOperation, AbstractAnswer> {

    protected final boolean stopIfFails;

    public PreprocessQueryTransformer() {
        this(false);
    }

    public PreprocessQueryTransformer(boolean stopIfFails) {
        this.stopIfFails = stopIfFails;
    }

    @Override
    public ProcessObjectsOperation transformOperation(QueryObjectOperation origOperation) {
        return ProcessObjectsOperation.createFrom(origOperation);
    }

    /**
     * If the answer returned from this step was successful, it should contain exactly one query object; this
     *  object is substituted in the original query operation.
     * @param origOperation original operation that entered the processing chain
     * @param processedOperation in this case, this is the same instance as the original operation
     * @param returnedAnswer answer from the processed transformed operation
     * @return
     */
    @Override
    public QueryObjectOperation updateOrigOperation(QueryObjectOperation origOperation, ProcessObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (failed(returnedAnswer)) {
            return origOperation;
        }
        origOperation.getParams().setField(QueryObject.QUERY_OBJECT_FIELD, ((ProcessObjectsAnswer) returnedAnswer).getAnswerObjects()[0]);
        return origOperation;
    }

    private boolean failed(AbstractAnswer returnedAnswer) {
        return (! returnedAnswer.wasSuccessful() || (! (returnedAnswer instanceof ProcessObjectsAnswer))
                || (((ProcessObjectsAnswer) returnedAnswer).getAnswerObjects().length != 1));
    }

    @Override
    public AbstractAnswer updateAnswer(QueryObjectOperation origOperation, ProcessObjectsOperation processedOperation, AbstractAnswer retAnswer) {
        if (stopIfFails && failed(retAnswer)) {
            return updateAnswerFail(retAnswer);
        }
        return null;
    }

    public static AbstractAnswer updateAnswerFail(AbstractAnswer retAnswer) {
        String errorDesc;
        if ((retAnswer instanceof ProcessObjectsAnswer) && ((ProcessObjectsAnswer) retAnswer).hasSkipped()) {
            errorDesc = ((ProcessObjectsAnswer) retAnswer).getFirstSkippedStatus().getErrorDesc();
        } else {
            errorDesc = retAnswer.getStatus().getErrorDesc();
        }
        return AbstractAnswer.createAnswer(new ProcessingStatus(ProcessObjectsAnswer.ERROR_PROCESSING, errorDesc));
    }

}
