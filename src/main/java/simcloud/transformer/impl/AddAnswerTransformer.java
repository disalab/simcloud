/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.operation.AbstractOperation;
import messif.operation.answer.AbstractAnswer;
import simcloud.cache.OperationCache;
import simcloud.transformer.OperationTransformerChain;

/**
 * This transformer adds answer to the query after it's processed
 */
public class AddAnswerTransformer<O extends AbstractOperation> implements OperationTransformerChain<O,O, AbstractAnswer> {

    @Override
    public O transformOperation(O origOperation) {
        return origOperation;
    }

    @Override
    public O updateOrigOperation(O origOperation, O processedOperation, AbstractAnswer returnedAnswer) {
        // and also add the answer to the "ANSWER" field (for caching)
        origOperation.getParams().setField(OperationCache.ANSWER_PARAM, returnedAnswer);
        return origOperation;
    }

    @Override
    public AbstractAnswer updateAnswer(O origOperation, O processedOperation, AbstractAnswer returnedAnswer) {
        return returnedAnswer;
    }
}
