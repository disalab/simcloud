/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.operation.AbstractOperation;
import messif.operation.OperationBuilder;
import messif.operation.answer.AbstractAnswer;
import messif.operation.params.CandidateIDSetQueue;
import messif.record.ModifiableRecord;
import simcloud.transformer.OperationTransformerChain;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * This transformer adds the queue of Set<ID> into a (typically) self-join operation. Such operation can be than processed
 *  in parallel by index like PPP-Codes and key-value store like MapDB.
 */
public class AddSimJoinCandTransformer<O extends AbstractOperation> implements OperationTransformerChain<O,O, AbstractAnswer> {

    protected final Class<O> actualOpClass;

    public AddSimJoinCandTransformer(Class<O> actualOpClass) {
        this.actualOpClass = actualOpClass;
    }

    @Override
    public O transformOperation(O origOperation) {
        ModifiableRecord origParams = origOperation.getParams();
        origParams.setField(CandidateIDSetQueue.ID_SET_QUEUE_PARAM, new LinkedBlockingQueue<>());
        return OperationBuilder.build(origParams, actualOpClass, false, CandidateIDSetQueue.class);
    }

    @Override
    public O updateOrigOperation(O origOperation, O processedOperation, AbstractAnswer returnedAnswer) {
        // clean the candidate ID params
        processedOperation.getParams().removeField(CandidateIDSetQueue.ID_SET_QUEUE_PARAM);
        return origOperation;
    }

    @Override
    public AbstractAnswer updateAnswer(O origOperation, O processedOperation, AbstractAnswer returnedAnswer) {
        return returnedAnswer;
    }
}
