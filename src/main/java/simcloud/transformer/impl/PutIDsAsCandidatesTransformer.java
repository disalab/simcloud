/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.data.DataObject;
import messif.operation.params.CandidateIDQueue;
import messif.operation.params.DataObjects;
import messif.operation.search.KNNRankObjectsOperation;

/**
 * This transformer adds the queue of IDs to be refined into the KNN operation. Such operation can be than processed
 *  in parallel by index like PPP-Codes and key-value store like MapDB. In case the operation is also instance of
 *  {@link DataObjects} (e.g. {@link messif.operation.search.KNNRankObjectsOperation})
 *  then the object IDs from the {@link DataObjects#OBJECTS_FIELD} field are added to the ID queue.
 */
public class PutIDsAsCandidatesTransformer extends AddCandidateIDQueueTransformer<KNNRankObjectsOperation> {

    public PutIDsAsCandidatesTransformer() {
        this(KNNRankObjectsOperation.class);
    }

    public PutIDsAsCandidatesTransformer(Class<KNNRankObjectsOperation> actualOpClass) {
        super(actualOpClass);
    }

    @Override
    public KNNRankObjectsOperation transformOperation(KNNRankObjectsOperation origOperation) {
        final KNNRankObjectsOperation retVal = CandidateIDQueue.addCandidateSetParams(actualOpClass, origOperation, origOperation.getObjectCount());
        for (DataObject dataObject : origOperation.getObjects()) {
            try {
                ((CandidateIDQueue) retVal).getIDQueue().put(dataObject.getID());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        ((CandidateIDQueue) retVal).getIDQueue().add(CandidateIDQueue.END_OF_STREAM);
        return retVal;
    }
}
