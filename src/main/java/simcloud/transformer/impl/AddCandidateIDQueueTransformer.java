/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.operation.AbstractOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.params.ApproximateSearch;
import messif.operation.params.CandidateIDQueue;

/**
 * This transformer adds the queue of IDs to be refined into the KNN operation. Such operation can be than processed
 *  in parallel by index like PPP-Codes and key-value store like MapDB. In case the operation is also instance of
 *  {@link messif.operation.params.DataObjects} (e.g. {@link messif.operation.search.KNNRankObjectsOperation})
 *  then the object IDs from the {@link messif.operation.params.DataObjects#OBJECTS_FIELD} field are added to the ID queue.
 */
public class AddCandidateIDQueueTransformer<O extends AbstractOperation> extends AddAnswerTransformer<O> {

    /** Default size of the candidate set used if not specified. Default: 3000. */
    public final static int CAND_SET_SIZE_DEFAULT = 3000;

    protected final Class<O> actualOpClass;

    public AddCandidateIDQueueTransformer(Class<O> actualOpClass) {
        this.actualOpClass = actualOpClass;
    }

    @Override
    public O transformOperation(O origOperation) {
        int candSetLocal = CAND_SET_SIZE_DEFAULT;
        if (origOperation instanceof ApproximateSearch) {
            ApproximateSearch approx = (ApproximateSearch) origOperation;
            if (approx.getApproxType() == ApproximateSearch.Type.ABS_OBJ_COUNT && approx.getApproxParam() > 0) {
                candSetLocal = approx.getApproxParam();
            }
        }
        return CandidateIDQueue.addCandidateSetParams(actualOpClass, origOperation, candSetLocal);
    }

    @Override
    public O updateOrigOperation(O origOperation, O processedOperation, AbstractAnswer returnedAnswer) {
        origOperation = super.updateOrigOperation(origOperation, processedOperation, returnedAnswer);
        // clean the candidate ID params
        processedOperation.getParams().removeField(CandidateIDQueue.ID_QUEUE_PARAM);

        return origOperation;
    }

    @Override
    public AbstractAnswer updateAnswer(O origOperation, O processedOperation, AbstractAnswer returnedAnswer) {
        return returnedAnswer;
    }
}
