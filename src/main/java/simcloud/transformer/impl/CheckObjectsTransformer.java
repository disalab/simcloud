/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.data.DataObject;
import messif.operation.answer.AbstractAnswer;
import messif.operation.OperationBuilder;
import messif.operation.crud.GetObjectsOperation;
import messif.operation.crud.InsertOperation;
import messif.operation.crud.ModifyingOperation;
import messif.operation.crud.answer.InsertAnswer;
import messif.operation.params.DataObjects;
import messif.operation.answer.ListingAnswer;
import simcloud.transformer.OperationTransformerChain;

import java.util.HashSet;
import java.util.Set;

/**
 * Given an insert operation, execute it normally but update the original operation based on the answer from this step
 *   (leave only objects that were actually inserted in this step).
 * @author david
 */
public class CheckObjectsTransformer implements OperationTransformerChain<InsertOperation, GetObjectsOperation, ListingAnswer> {

    @Override
    public GetObjectsOperation transformOperation(InsertOperation origOperation) {
        return OperationBuilder.create(GetObjectsOperation.class).addParam(GetObjectsOperation.OBJECTS_FIELD, origOperation.getObjects())
                .addParam(GetObjectsOperation.FIELDS_TO_RETURN_FIELD, ModifyingOperation.DEFAULT_LIST).build();
    }

    @Override
    public InsertOperation updateOrigOperation(InsertOperation origOperation, GetObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        // remove the returned objects from the InsertOperation and store them into a information field of the answer?
        if (returnedAnswer.wasSuccessful() && (returnedAnswer instanceof ListingAnswer)
                && (((ListingAnswer) returnedAnswer).getAnswerCount() > 0)) {
            // put all returned IDs into a HashSet
            Set<String> existingIDs = new HashSet<>(((ListingAnswer) returnedAnswer).getAnswerCount());
            for (DataObject returnedObj : ((ListingAnswer) returnedAnswer).getAnswerObjects()) {
                existingIDs.add(returnedObj.getID());
            }
            // iterate over objects to insert and remove the existing ones
            DataObject[] objectsToInsert = new DataObject[origOperation.getObjectCount() - existingIDs.size()];
            int i = 0;
            for (DataObject obj : origOperation.getObjects()) {
                if (existingIDs.contains(obj.getID())) {

                } else {
                    objectsToInsert[i++] = obj;
                }
            }
            origOperation.getParams().setField(DataObjects.OBJECTS_FIELD, objectsToInsert);
        }
        return origOperation;
    }

    @Override
    public ListingAnswer updateAnswer(InsertOperation origOperation, GetObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (returnedAnswer.wasSuccessful() && (returnedAnswer instanceof ListingAnswer)
                && (((ListingAnswer) returnedAnswer).getAnswerCount() > 0)) {
            final InsertOperation.Helper helper = new InsertOperation.Helper(origOperation);
            for (DataObject dataObject : ((ListingAnswer) returnedAnswer).getAnswerObjects()) {
                helper.skipObject(dataObject, InsertAnswer.OBJECT_DUPLICATE);
            }
            helper.errorCodeHelper.updateErrorCode(InsertAnswer.OBJECT_DUPLICATE);
            return helper.getAnswer();
        }
        return null;
    }
}
