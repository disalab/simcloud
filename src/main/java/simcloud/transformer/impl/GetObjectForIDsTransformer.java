/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.data.DataObject;
import messif.operation.OperationBuilder;
import messif.operation.ReturnDataOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.crud.GetObjectsOperation;
import simcloud.transformer.OperationTransformerChain;

/**
 * This transformer is used, e.g., for the {@link messif.operation.search.GetRandomObjectsOperation} processing: after
 *  the operation is processed on the PPP-Codes index and object IDs are stored in the "id_list" field of the original
 *  operation (see {@link IdsToOperation}, this transformer is used to retrieve additional data fields
 *  for the objects from the key-value store.
 * @author david
 */
public class GetObjectForIDsTransformer implements OperationTransformerChain<ReturnDataOperation, GetObjectsOperation, AbstractAnswer> {

    @Override
    public GetObjectsOperation transformOperation(ReturnDataOperation origOperation) {
        return OperationBuilder.create(GetObjectsOperation.class).addParam(GetObjectsOperation.OBJECTS_FIELD,
                origOperation.getParams().getField(IdsToOperation.ID_LIST_FIELD, DataObject[].class, new DataObject[0]))
                .addParam(ReturnDataOperation.FIELDS_TO_RETURN_FIELD, origOperation.fieldsToReturn()).build();
    }

    @Override
    public ReturnDataOperation updateOrigOperation(ReturnDataOperation origOperation, GetObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        origOperation.getParams().removeField(IdsToOperation.ID_LIST_FIELD);
        return origOperation;
    }

    @Override
    public AbstractAnswer updateAnswer(ReturnDataOperation origOperation, GetObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        return returnedAnswer;
    }
}
