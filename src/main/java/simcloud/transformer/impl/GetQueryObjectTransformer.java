/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.data.DataObject;
import messif.operation.answer.AbstractAnswer;
import messif.operation.OperationBuilder;
import messif.operation.crud.GetObjectsOperation;
import messif.operation.search.QueryObjectOperation;
import messif.operation.answer.ListingAnswer;
import simcloud.transformer.OperationTransformerChain;

/**
 * Given an operation of type {@link messif.operation.search.QueryObjectOperation} (e.g. {@link messif.operation.search.KNNOperation}),
 *  this transformer creates a {@link messif.operation.crud.GetObjectsOperation} and tries to retrieve additional data
 *  fields of the query object from the key-value store.
 * @author david
 */
public class GetQueryObjectTransformer implements OperationTransformerChain<QueryObjectOperation, GetObjectsOperation, ListingAnswer> {

    @Override
    public GetObjectsOperation transformOperation(QueryObjectOperation origOperation) {
        return OperationBuilder.create(GetObjectsOperation.class).addParam(GetObjectsOperation.OBJECTS_FIELD, new DataObject[] {origOperation.getQueryObject()}).build();
    }

    @Override
    public QueryObjectOperation updateOrigOperation(QueryObjectOperation origOperation, GetObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (returnedAnswer instanceof ListingAnswer && returnedAnswer.wasSuccessful() && ((ListingAnswer) returnedAnswer).getAnswerCount() == 1) {
            origOperation.getParams().setField(QueryObjectOperation.QUERY_OBJECT_FIELD, ((ListingAnswer)returnedAnswer).getAnswerObjects()[0]);
        }
        return origOperation;
    }

    @Override
    public ListingAnswer updateAnswer(QueryObjectOperation origOperation, GetObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        return null;
    }
}
