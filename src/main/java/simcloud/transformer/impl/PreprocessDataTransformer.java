/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.operation.ProcessObjectsOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.params.DataObjects;
import simcloud.transformer.OperationTransformerChain;

/**
 * Given a query operation, the queyr object is processed by a given processor and replaced in the query operation.
 * @author david
 */
public class PreprocessDataTransformer<T extends ProcessObjectsOperation>
        implements OperationTransformerChain<T, ProcessObjectsOperation, AbstractAnswer> {

    @Override
    public ProcessObjectsOperation transformOperation(T origOperation) {
        return ProcessObjectsOperation.createFrom(origOperation);
    }

    /**
     * The successfully substituted objects from this step are replaced for the original {@link DataObjects#OBJECTS_FIELD}.
     * @param origOperation original operation that entered the processing chain
     * @param processedOperation in this case, this is the same instance as the original operation
     * @param returnedAnswer answer from the processed transformed operation
     * @return
     */
    @Override
    public T updateOrigOperation(T origOperation, ProcessObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (! (returnedAnswer instanceof ProcessObjectsAnswer)) {
            return origOperation;
        }
        // now replace the objects to be inserted with the result of this pre-processing step
        // the wrong (skipped) objects are not to be inserted
        origOperation.getParams().setField(DataObjects.OBJECTS_FIELD, ((ProcessObjectsAnswer) returnedAnswer).getAnswerObjects());
        return origOperation;
    }

    /**
     * The objects that failed this step processing (and were removed from the insert operation) are returned in an
     *  answer so that this answer can be merged into the global "insert operation" answer.
     * @param origOperation original operation that entered the processing chain
     * @param processedOperation the transformed operation transformed
     * @param returnedAnswer answer from the processed transformed operation
     * @return
     */
    @Override
    public AbstractAnswer updateAnswer(T origOperation, ProcessObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (returnedAnswer instanceof ProcessObjectsAnswer && (((ProcessObjectsAnswer) returnedAnswer).getSkippedObjects() != null)) {
            // return an answer that contains just the errors (skipped objects)
            return ProcessObjectsAnswer.createFailed(returnedAnswer.getStatus(), ((ProcessObjectsAnswer) returnedAnswer).getSkippedObjects());
        }
        return null;
    }
}
