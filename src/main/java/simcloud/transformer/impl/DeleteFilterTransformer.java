/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.operation.answer.AbstractAnswer;
import messif.operation.OperationBuilder;
import messif.operation.ReturnDataOperation;
import messif.operation.crud.DeleteOperation;
import messif.operation.crud.InsertOperation;
import messif.operation.answer.ListingAnswer;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import simcloud.transformer.OperationTransformerChain;

import java.util.Arrays;

/**
 * Given a delete operation, execute it normally but update the original operation based on the answer from this step
 *   (leave only objects that were actually inserted in this step).
 * @author david
 */
public class DeleteFilterTransformer implements OperationTransformerChain<DeleteOperation, DeleteOperation, ListingAnswer> {

    @Override
    public DeleteOperation transformOperation(DeleteOperation origOperation) {
        // request that the answer contains full objects (not just IDs)
        if (! Arrays.equals(origOperation.fieldsToReturn(), ReturnDataOperation.DEFAULT_LIST)) {
            // create A NEW operation (always)
            final ModifiableRecord newParams = new ModifiableRecordImpl(origOperation.getParams());
            newParams.setField(ReturnDataOperation.FIELDS_TO_RETURN_FIELD, ReturnDataOperation.DEFAULT_LIST);
            return OperationBuilder.build(newParams, DeleteOperation.class, false);
        }
        return origOperation;
    }

    @Override
    public DeleteOperation updateOrigOperation(DeleteOperation origOperation, DeleteOperation processedOperation, AbstractAnswer returnedAnswer) {
        // continue deleting only those objects returned in the first step
        if (returnedAnswer instanceof ListingAnswer) {
            origOperation.getParams().setField(InsertOperation.OBJECTS_FIELD, ((ListingAnswer) returnedAnswer).getAnswerObjects());
        }
        return origOperation;
//        final ModifiableRecord origParams = (origOperation.getParams() instanceof ModifiableRecord) ?
//                (ModifiableRecord) origOperation.getParams() : new ModifiableRecordImpl(origOperation.getParams());
//        origParams.setField(InsertOperation.OBJECTS_FIELD, returnedAnswer.getAnswerObjects());
//
//        return OperationBuilder.build(origParams, DeleteOperation.class, false);
    }

    @Override
    public ListingAnswer updateAnswer(DeleteOperation origOperation, DeleteOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (returnedAnswer instanceof ListingAnswer) {
            return ((ListingAnswer)returnedAnswer);
        }
        return null;
    }
}
