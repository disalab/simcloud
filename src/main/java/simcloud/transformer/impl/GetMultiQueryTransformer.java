/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.data.DataObject;
import messif.operation.OperationBuilder;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ListingAnswer;
import messif.operation.crud.GetObjectsOperation;
import messif.operation.search.MultiQueryObjectOperation;
import simcloud.transformer.OperationTransformerChain;

/**
 * Given an operation of type {@link MultiQueryObjectOperation} (e.g. {@link messif.operation.search.MultiKNNOperation}),
 *  this transformer creates a {@link GetObjectsOperation} and tries to retrieve additional data
 *  fields of the query objects from the key-value store.
 * @author david
 */
public class GetMultiQueryTransformer implements OperationTransformerChain<MultiQueryObjectOperation, GetObjectsOperation, AbstractAnswer> {

    protected final boolean stopIfFails;

    public GetMultiQueryTransformer() {
        this(false);
    }

    public GetMultiQueryTransformer(boolean stopIfFails) {
        this.stopIfFails = stopIfFails;
    }

    @Override
    public GetObjectsOperation transformOperation(MultiQueryObjectOperation origOperation) {
        return OperationBuilder.create(GetObjectsOperation.class).addParam(GetObjectsOperation.OBJECTS_FIELD, origOperation.getQueryObjects()).build();
    }

    @Override
    public MultiQueryObjectOperation updateOrigOperation(MultiQueryObjectOperation origOperation, GetObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        // match the retrieved objects to the query objects
        if (returnedAnswer instanceof ListingAnswer && returnedAnswer.wasSuccessful() && ((ListingAnswer) returnedAnswer).getAnswerCount() > 0) {
            // simply replace the query objects
            DataObject [] enrichedQueryObjects;
            if (((ListingAnswer) returnedAnswer).getAnswerObjects().length == origOperation.getQueryObjectCount()) {
                enrichedQueryObjects = ((ListingAnswer) returnedAnswer).getAnswerObjects();
            } else {
                enrichedQueryObjects = origOperation.getQueryObjects();
                for (int i = 0; i < enrichedQueryObjects.length; i++) {
                    // find corresponding returned object
                    for (DataObject returnedObj : ((ListingAnswer) returnedAnswer).getAnswerObjects()) {
                        if (returnedObj.getID() != null && returnedObj.getID().equals(enrichedQueryObjects[i].getID())) {
                            enrichedQueryObjects[i] = returnedObj;
                            break;
                        }
                    }
                }
            }
            origOperation.getParams().setField(MultiQueryObjectOperation.QUERY_OBJECTS_FIELD, enrichedQueryObjects);
        }
        return origOperation;
    }

    @Override
    public AbstractAnswer updateAnswer(MultiQueryObjectOperation origOperation, GetObjectsOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (stopIfFails && (! returnedAnswer.wasSuccessful() || (! (returnedAnswer instanceof ListingAnswer))
                || (((ListingAnswer) returnedAnswer).getAnswerObjects().length != origOperation.getQueryObjectCount()))) {
            return PreprocessQueryTransformer.updateAnswerFail(returnedAnswer);
        }
        return null;
    }
}
