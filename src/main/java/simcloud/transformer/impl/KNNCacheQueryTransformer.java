/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer.impl;

import messif.data.DataObject;
import messif.operation.answer.AbstractAnswer;
import messif.operation.search.KNNOperation;
import messif.operation.answer.RankedAnswer;
import simcloud.transformer.OperationTransformerChain;

/**
 * Created by david on 17/01/17.
 */
public class KNNCacheQueryTransformer implements OperationTransformerChain<KNNOperation, KNNOperation, RankedAnswer> {

    /** Name of a field to temporarily carry info about the original "k" set by the user. */
    public static final String ORIGINAL_K = "original_k";

    protected final int defaultMaxK;

    public KNNCacheQueryTransformer(int defaultMaxK) {
        this.defaultMaxK = defaultMaxK;
    }

    /**
     * This method transforms THE ORIGINAL operation and sends it to this step's processor (typically cache).
     * @param origOperation original operation that entered the processing chain
     * @return operation to be executed on the underlying cache; it is just the modified original operation
     */
    @Override
    public KNNOperation transformOperation(KNNOperation origOperation) {
        int originalK = origOperation.getK();
        // set the actual "k" to a larger value (for caching)
        origOperation.setK(Math.max(originalK + origOperation.getFrom(), defaultMaxK));
        // and store the original "k"
        origOperation.getParams().setField(ORIGINAL_K, originalK);
        return origOperation;
    }

    /**
     * The original operation has already been modified by the {@link #transformOperation(KNNOperation)}
     * @param origOperation original operation that entered the processing chain
     * @param processedOperation
     * @param returnedAnswer answer from the processed transformed operation
     * @return
     */
    @Override
    public KNNOperation updateOrigOperation(KNNOperation origOperation, KNNOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (returnedAnswer == null || !(returnedAnswer instanceof RankedAnswer) || !(returnedAnswer.wasSuccessful())) {
            return origOperation;
        }
        // if the answer was returned, the original operation should be updated to has it's original parameters
        origOperation.setK(processedOperation.getParams().getField(ORIGINAL_K, Integer.TYPE));
        origOperation.getParams().removeField(ORIGINAL_K);
        return origOperation;
    }

    @Override
    public RankedAnswer updateAnswer(KNNOperation origOperation, KNNOperation processedOperation, AbstractAnswer returnedAnswer) {
        if (returnedAnswer == null || !(returnedAnswer instanceof RankedAnswer) ||
                (processedOperation.getFrom() == processedOperation.getParams().getField(ORIGINAL_K, Integer.TYPE) && processedOperation.getFrom() == 0)) {
            if (!(returnedAnswer instanceof RankedAnswer)) {
                return null;
            }
            return (RankedAnswer) returnedAnswer;
        }
        // modify the answer to return the originally required "k" objects starting from the "from" parameter
        final DataObject[] answerObjects = ((RankedAnswer)returnedAnswer).getAnswerObjects();
        final float[] answerDistances = ((RankedAnswer)returnedAnswer).getAnswerDistances();
        int maxPosition = Math.min(((RankedAnswer)returnedAnswer).getAnswerCount(), processedOperation.getFrom() + processedOperation.getParams().getField(ORIGINAL_K, Integer.TYPE));

        RankedAnswer.Builder answerBuilder = new RankedAnswer.Builder(processedOperation, RankedAnswer.class);
        for (int i = processedOperation.getFrom(); i < maxPosition; i++) {
            answerBuilder.add(answerObjects[i], answerDistances[i], null);
        }
        answerBuilder.errorCodeHelper.updateErrorCode(returnedAnswer.getStatus());
        return answerBuilder.getAnswer();
    }
}
