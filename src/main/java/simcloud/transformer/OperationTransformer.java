/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.transformer;


import messif.operation.AbstractOperation;

/**
 * 
 * @author xnovak8
 * @param <O> type of the original operation
 * @param <T> type of the next step operation
 */
public interface OperationTransformer<O extends AbstractOperation, T extends AbstractOperation> {

    /**
     * Given an operation from the outside and result from the previous step in the chain, this method
     *  creates an operation for the next step.
     * @param origOperation original operation that entered the processing chain
     * @return operation to be processed within the next step
     */
    T transformOperation(O origOperation);

}
