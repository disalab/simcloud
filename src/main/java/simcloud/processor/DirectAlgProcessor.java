/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.processor;


import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationEvaluator;
import messif.operation.processing.impl.OneStepProcessor;
import simcloud.SimCloudManager;

public class DirectAlgProcessor<T extends AbstractOperation, P extends AbstractAnswer> extends OneStepProcessor<T, P> {

    /**
     * Algorithm to process the operation on.
     */
    protected final OperationEvaluator algorithm;

    protected P answer;
    
    /**
     * Creates this processor given an algorithm and operation to be executed on it.
     *
     * @param algorithm specific algorithm on which to execute the operation
     * @param operation operation to be executed
     */
    public DirectAlgProcessor(OperationEvaluator algorithm, T operation) {
         super(operation);
        this.algorithm = algorithm;
    }

    @Override
    public P finish() {
        return answer;
    }

    @Override
    protected void process() throws AlgorithmMethodException {
        AbstractAnswer abstractAnswer;
        try {
            abstractAnswer = algorithm.evaluate(getOperation());
        } catch (Exception e) {
            SimCloudManager.logger.severe(e.getMessage());
            throw new AlgorithmMethodException(e);
        }
        try {
            answer = (P) abstractAnswer;
        } catch (ClassCastException e) {
            answer = null;
        }
    }
}
