/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.processor;

import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.processing.OperationProcessor;
import simcloud.config.ChainItem;
import simcloud.config.ProcessorConfig;

import java.util.List;

/**
 * Generic processor to realize complex operation processing on several sub-processors. These
 *  processors are grouped into "batches", each of which is parallelized but next batch is
 *  started only when the current batch finishes.
 * @author xnovak8
 */
public class ChainProcessor<O extends AbstractOperation, A extends AbstractAnswer> implements OperationProcessor<O, A> {

    protected final List<ChainItem<O>> items;

    /**
     * An answer provider to be used for merging partial answers and providing the final one. It can be null (in this
     *  case, answer from the last operation processed is returned).
     */
    protected final AnswerProvider<A> answerProvider;

    /**
     * In case, the answer merger is empty, this field stores the last returned answer.
     */
    protected A finalAnswer;

    /** Index of the currently processed batch. */
    protected int nextExecutorIndex = 0;

    /** Operation being processed by this class - it can change during the processing between steps. */
    protected O operation;

    /**
     * Create this complex processor given the thread pool, the initiating operation and list of processors;
     *  all processors are applied to the operation one after the other.
     * @param operation operation to process by this processor
     * @param items
     */
    public ChainProcessor(O operation, List<ChainItem<O>> items) {
        this(operation, items, null);
    }

    /**
     * Create this complex processor given the thread pool, the initiating operation and list of processors;
     *  all processors are applied to the operation one after the other.
     * @param operation operation to process by this processor
     * @param items
     */
    public ChainProcessor(O operation, List<ChainItem<O>> items, AnswerProvider<A> answerProvider) {
        this.operation = operation;
        this.items = items;
        this.answerProvider = answerProvider;
    }

    @Override
    public boolean processStep() throws InterruptedException, AlgorithmMethodException, CloneNotSupportedException {
        if (nextExecutorIndex >= items.size()) {
            return false;
        }

        try {
            // get next operation config
            final ChainItem<O> chainItem = items.get(nextExecutorIndex);
            nextExecutorIndex ++;
            return processItem(chainItem);
        } catch (Exception e) {
            throw new AlgorithmMethodException(e);
        }
    }

    protected boolean processItem(final ChainItem<O> chainItem) throws Exception {
        final ProcessorConfig<O, ?> configuration = chainItem.getConfig();
        final OperationProcessor<? super O, ?> operationProcessor = configuration.initProcessor(operation);
        final AbstractAnswer answer = configuration.getExecutor().execute(operationProcessor);

        return updateAfterStep(chainItem, operationProcessor, answer);
    }

    protected boolean updateAfterStep(final ChainItem<O> chainItem, final OperationProcessor<? super O, ?> operationProcessor, final AbstractAnswer answer) {
        // if this is the last step
        boolean retVal = true;
        //if ((chainItem.isFinal() && answer != null && answer.wasSuccessful()) || (nextExecutorIndex >= items.size())) {
        if ((chainItem.isFinal() && answer != null) || (nextExecutorIndex >= items.size())) {
            if (answer != null && answerProvider == null) {
                finalAnswer = (A) answer;
            }
            retVal = false;
        }
        // else, update the answer using the answer provider
        if (answerProvider != null && answer != null)
            answerProvider.updateAnswer(answer);
        // ... and update the operation
        operation = (O) operationProcessor.getOperation();
        return retVal;
    }

    @Override
    public O getOperation() {
        return operation;
    }

    @Override
    public A finish() {
        if (answerProvider != null)
            return answerProvider.getAnswer();
        return finalAnswer;
    }

}
