/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.processor;


import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationProcessor;
import messif.operation.processing.ProcessorExecutor;
import messif.operation.processing.impl.OneStepProcessor;
import simcloud.SimCloudManager;
import simcloud.config.ProcessorConfig;
import simcloud.transformer.OperationTransformerChain;

public class TransformEncapsulateProcessor<O extends AbstractOperation, T extends AbstractOperation, P extends AbstractAnswer, A extends AbstractAnswer> extends OneStepProcessor<O, A> {

    protected final OperationProcessor<T, P> directProcessor;

    protected final OperationTransformerChain<O, T, A> transformerFinal;

    final ProcessorExecutor executor;

    P answer;

    /**
     * Creates this processor given an algorithm and operation to be executed on it.
     *
     * @param operation operation to be executed
     */
    public TransformEncapsulateProcessor(final ProcessorConfig<T, P> innerConfig, O operation, OperationTransformerChain<O, T, A> transformerFinal) {
        super(operation);
        this.directProcessor = (OperationProcessor<T, P>) innerConfig.initProcessor(transformerFinal.transformOperation(operation));
        this.executor = innerConfig.getExecutor();
        this.transformerFinal = transformerFinal;
    }

    @Override
    public A finish() {
        return transformerFinal.updateAnswer(operation, directProcessor.getOperation(), answer);
    }

    @Override
    public O getOperation() {
        return transformerFinal.updateOrigOperation(super.getOperation(), directProcessor.getOperation(), answer);
    }

    @Override
    protected void process() throws AlgorithmMethodException {
        try {
            this.answer = executor.execute(directProcessor);
        } catch (Exception e) {
            SimCloudManager.logger.severe(e.getMessage());
            throw new AlgorithmMethodException(e);
        }
    }
}
