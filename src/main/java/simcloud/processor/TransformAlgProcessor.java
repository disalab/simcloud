/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.processor;


import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationEvaluator;
import messif.operation.processing.impl.OneStepProcessor;
import simcloud.SimCloudManager;
import simcloud.transformer.OperationTransformerChain;

/**
 * A complex operation processor that takes an operation, transforms it using a given {@link OperationTransformerChain},
 *  executes the transformed operation on a given algorithm, modifies the returned answer using method
 *  {@link OperationTransformerChain#updateAnswer(AbstractOperation, AbstractOperation, AbstractAnswer)}
 *  and this answer is finally returned.
 *  The {@link #getOperation()} method (if called after the processing) returns operation modified by calling
 *  {@link OperationTransformerChain#updateOrigOperation(AbstractOperation, AbstractOperation, AbstractAnswer)}.
 * @param <O> type of the original operation
 * @param <T> type of the transformed operation actually processed on the given algorithm
 * @param <P> type of the intermediate answer returned by the algorithm
 * @param <A> type of the final answer produced by the
 *  {@link OperationTransformerChain#updateOrigOperation(AbstractOperation, AbstractOperation, AbstractAnswer)} method.
 */
public class TransformAlgProcessor<O extends AbstractOperation, T extends AbstractOperation, P extends AbstractAnswer, A extends AbstractAnswer>
        extends OneStepProcessor<O, A> {

    /**
     * The actual processor that processes the TRANSFORMED operation and returns answer of type {@link P}.
     */
    protected final DirectAlgProcessor<T, P> directProcessor;

    /**
     * The transformer to be applied before the operation is processed and then the answer and the original is updated
     *  by this transformer after the processing.
     */
    protected final OperationTransformerChain<O, T, A> chainTransformer;

    /**
     * The answer from the {@link #directProcessor} (before calling
     *  {@link OperationTransformerChain#updateAnswer(AbstractOperation, AbstractOperation, AbstractAnswer)}).
     */
    protected P intermediateAnswer;

    /**
     * Creates this processor given an algorithm and operation to be executed on it.
     *
     * @param algorithm specific algorithm on which to execute the operation
     * @param operation operation to be executed
     */
    public TransformAlgProcessor(OperationEvaluator algorithm, O operation, OperationTransformerChain<O, T, A> chainTransformer) {
        super(operation);
        directProcessor = new DirectAlgProcessor<>(algorithm, chainTransformer.transformOperation(operation));
        this.chainTransformer = chainTransformer;
    }

    @Override
    public A finish() {
        if (intermediateAnswer == null)
            intermediateAnswer = directProcessor.finish();
        return chainTransformer.updateAnswer(operation, directProcessor.getOperation(), intermediateAnswer);
    }

    @Override
    public O getOperation() {
        if (intermediateAnswer == null)
            return super.getOperation();
        return chainTransformer.updateOrigOperation(super.getOperation(), directProcessor.getOperation(), intermediateAnswer);
    }

    @Override
    protected void process() throws AlgorithmMethodException {
        try {
            directProcessor.process();
            intermediateAnswer = directProcessor.finish();
        } catch (Exception e) {
            SimCloudManager.logger.severe(e.getMessage());
            throw new AlgorithmMethodException(e);
        }
    }
}
