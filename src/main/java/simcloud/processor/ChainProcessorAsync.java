/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.processor;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.processing.AsyncOperationProcessor;
import messif.operation.processing.OperationProcessor;
import simcloud.config.ChainItem;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Generic processor to realize complex operation processing on several sub-processors. These 
 *  sub-processors are executed in parallel.
 * @author xnovak8
 */
public class ChainProcessorAsync<O extends AbstractOperation, A extends AbstractAnswer> extends ChainProcessor<O, A> implements AsyncOperationProcessor<O, A> {

    /**
     * Create this complex processor given the thread pool, the initiating operation and list of processors;
     * all processors are applied to the operation one after the other.
     *
     * @param operation  operation to process by this processor
     * @param chainItems
     */
    public ChainProcessorAsync(O operation, List<ChainItem<O>> chainItems) {
        super(operation, chainItems);
    }

    /**
     * Create this complex processor given the thread pool, the initiating operation and list of processors;
     * all processors are applied to the operation one after the other.
     *
     * @param operation  operation to process by this processor
     * @param chainItems
     */
    public ChainProcessorAsync(O operation, List<ChainItem<O>> chainItems, AnswerProvider<A> answerProvider) {
        super(operation, chainItems, answerProvider);
    }

    @Override
    public Callable<Void> processStepAsynchronously() throws InterruptedException {
        if (nextExecutorIndex >= items.size()) {
            return null;
        }

        // get next operation executor
        final ChainItem<O> chainItem = items.get(nextExecutorIndex);
        nextExecutorIndex ++;
        return () -> {
            ChainProcessorAsync.super.processItem(chainItem);
            return null;
        };
    }

    @Override
    protected synchronized boolean updateAfterStep(ChainItem<O> chainItem, OperationProcessor<? super O, ?> operationProcessor, AbstractAnswer answer) {
        // if the step was marked as "final", consider its answer
        if (chainItem.isFinal()) {
            if (answer != null) {
                if (answerProvider == null)
                    finalAnswer = (A) answer;
                else
                    answerProvider.updateAnswer(answer);
            }
        }
        return true;
    }

    //
//    protected final synchronized NavigationProcessor<? extends AbstractOperation> getNextProcessor(final OperationExecutor executor) {
//        NavigationProcessor<? extends AbstractOperation> nextProcessor = null;
//        try {
//            return nextProcessor = AbstractOperationExecutor.getProcessor(manager, operation, executor, previousProcessor);
//        } finally {
//            previousProcessor = nextProcessor;
//            nextExecutorIndex ++;
//        }
//    }
}
