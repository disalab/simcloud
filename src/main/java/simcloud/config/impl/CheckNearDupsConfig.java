/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config.impl;

import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.processing.OperationProcessor;
import messif.operation.processing.ProcessorExecutor;
import messif.operation.search.CheckNearDupsOperation;
import simcloud.SimCloudManager;
import simcloud.config.ProcessorConfig;

/**
 * A specific configuration to runs evaluation of a {@link CheckNearDupsOperation} on the manager itself using
 *   a default processor of the operation. It processes individual near-dups operations in parallel.
 */
public class CheckNearDupsConfig extends ProcessorConfig<CheckNearDupsOperation, ProcessObjectsAnswer> {

    private SimCloudManager manager;

    private ProcessorExecutor executor;

    public CheckNearDupsConfig() {
        super(CheckNearDupsOperation.class);
    }

    @Override
    public OperationProcessor<CheckNearDupsOperation, ProcessObjectsAnswer> initProcessor(CheckNearDupsOperation operation) {
        return new CheckNearDupsOperation.Processor(operation, manager);
    }

    @Override
    public ProcessorExecutor getExecutor() {
        return executor;
    }

    @Override
    public void setManager(SimCloudManager manager) {
        this.manager = manager;
        this.executor = new ProcessorExecutor.ParallelExecutor(manager.getThreadPool());
    }
}
