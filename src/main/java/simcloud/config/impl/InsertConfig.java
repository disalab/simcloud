/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config.impl;

import messif.data.DataObject;
import messif.operation.crud.InsertOperation;
import messif.operation.crud.answer.InsertAnswer;
import messif.operation.params.Range;
import messif.operation.processing.OperationProcessor;
import messif.record.processing.RecordProcessor;
import simcloud.SimCloudManager;
import simcloud.config.ChainItem;
import simcloud.config.ProcessorConfig;
import simcloud.processor.ChainProcessor;
import simcloud.transformer.impl.CheckNearDupsTransformer;
import simcloud.transformer.impl.CheckObjectsTransformer;

import java.util.ArrayList;
import java.util.List;

/**
 * A specific configuration that
 *  1) first checks that the inserted objects are not already stored in the main key-value store
 *    (removes the ones that are in the key-value),
 *  2) then runs the extraction of the descriptors,
 *  3) if requested, it checks the objects for near-duplicates
 *  4) then check that there are some objects left
 *  5) stores the objects into the key-value store
 *  6) stores the objects into given indexes.
 */
public class InsertConfig extends ProcessorConfig<InsertOperation, InsertAnswer> {

    /** If this field in the Insert operation contains true, then the check of object existence is skipped. */
    public static final String SKIP_CHECK = "SKIP_CHECK";

    final List<ChainItem<InsertOperation>> chainItems;

    final List<ChainItem<InsertOperation>> chainCheckNearDups;

    final List<ChainItem<InsertOperation>> chainSkipCheck;

    public InsertConfig(RecordProcessor<DataObject> extractor, RecordProcessor<DataObject> tagPreprocessor,
                        String manager, String keyValueStore, String ... indexes) {
        super(InsertOperation.class);

        // create a list of chain items for a standard insert
        chainItems = new ArrayList<>();
        // check existing _id(s) in the key-value store
        chainItems.add(ChainItem.transformAlg(InsertOperation.class, keyValueStore, new CheckObjectsTransformer(), false));
        // apply the extractor to all data objects
        chainItems.add(ChainItem.givenConfig(new PreprocessDataConfig<>(InsertOperation.class, extractor, true), false));
        //chainItems.add(ChainItem.transformAlg(InsertOperation.class, extractor, new PreprocessDataTransformer(), false));
        // preprocess the keywords (tags) to create an array of integer hash values
        chainItems.add(ChainItem.givenConfig(new PreprocessDataConfig<>(InsertOperation.class, tagPreprocessor, false), false));

        // check that the list of objects is not empty now
        chainItems.add(CheckConditionItem.checkObjectsNotEmpty(InsertOperation.class));

        chainItems.add(ChainItem.directAlg(InsertOperation.class, keyValueStore, false));
        for (String index : indexes) {
            chainItems.add(ChainItem.directAlg(InsertOperation.class, index, false));
        }

        // create another chain of items that also check that there are no near duplicate items in the index
        chainCheckNearDups = new ArrayList<>(chainItems);
        chainCheckNearDups.add(3, ChainItem.transformAlg(InsertOperation.class, manager, new CheckNearDupsTransformer(5.0f), false));

        // create another chain of items that SKIPS the GetOperation check
        chainSkipCheck = new ArrayList<>(chainItems);
        chainSkipCheck.remove(0);
    }

    @Override
    public OperationProcessor<InsertOperation, InsertAnswer> initProcessor(InsertOperation operation) {
        // if the check by GetOperation should be skipped
        if (operation.getParams().getField(SKIP_CHECK, Boolean.TYPE, false)) {
            return new ChainProcessor<>(operation, chainSkipCheck, new InsertOperation.Helper(operation));
        }
        // use a different chain in case of the insert operation contains a parameter "radius"
        if (operation.getParams().containsField(Range.RADIUS_FIELD)) {
            return new ChainProcessor<>(operation, chainCheckNearDups, new InsertOperation.Helper(operation));
        }
        return new ChainProcessor<>(operation, chainItems, new InsertOperation.Helper(operation));
    }

    @Override
    public void setManager(SimCloudManager manager) {
        for (ChainItem<InsertOperation> chainItem : chainCheckNearDups) {
            chainItem.getConfig().setManager(manager);
        }
    }
}
