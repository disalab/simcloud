/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config.impl;

import messif.data.DataObject;
import messif.data.processing.ObjectsProcessorEvaluator;
import messif.operation.ProcessObjectsOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.processing.OperationProcessor;
import messif.record.processing.RecordProcessor;
import simcloud.SimCloudManager;
import simcloud.config.ProcessorConfig;
import simcloud.processor.TransformAlgProcessor;
import simcloud.transformer.impl.PreprocessDataTransformer;

/**
 * A specific configuration that takes an operation with "data objects" (typically CRUD operation) and
 *  applies given transformer (e.g. preprocessing of "tags") to each of the data object.
 */
public class PreprocessDataConfig<T extends ProcessObjectsOperation> extends ProcessorConfig<T, AbstractAnswer> {

    /**
     * Transformer to be applied on each data object.
     */
    final PreprocessDataTransformer transformer;

    final ObjectsProcessorEvaluator evaluator;

    /**
     *
     * @param tClass
     * @param recordProcessor
     * @param checkAfter if true, then the processing condition is checked also after the processing
     *                            - see {@link ObjectsProcessorEvaluator#checkConditionAfter}.
     */
    public PreprocessDataConfig(Class<T> tClass, RecordProcessor<DataObject> recordProcessor, boolean checkAfter) {
        super(tClass);
        this.transformer = new PreprocessDataTransformer();
        this.evaluator = new ObjectsProcessorEvaluator(recordProcessor, checkAfter);
    }

    @Override
    public OperationProcessor<? super T, ? extends AbstractAnswer> initProcessor(T operation) {
        return new TransformAlgProcessor<>(evaluator, operation, transformer);
    }

    @Override
    public void setManager(SimCloudManager manager) {
    }
}
