/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config.impl;

import messif.algorithm.AlgorithmMethodException;
import messif.operation.AbstractOperation;
import messif.operation.ProcessObjectsOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.processing.OperationProcessor;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.search.QueryObjectOperation;
import simcloud.SimCloudManager;
import simcloud.config.ChainItem;
import simcloud.config.ProcessorConfig;

import java.util.function.Predicate;

/**
 * A special chain item that check if the query object is valid (contains a specific field etc.). If not, empty answer
 *  is returned and the processing ends prematurely.
 */
public class CheckConditionItem<O extends AbstractOperation> implements ChainItem<O> {

    // check that the query object operation DOES NOT HAVE the specified field
    final Predicate<O> conditionToCheck;

    // and if it really does NOT have the field, return "wrong query object" answer
    final AbstractAnswer answerToReturn;

    // and if it really does NOT have the field, return "wrong query object" answer
    final Class<O> oClass;

    /**
     * Creates a new condition-checking chain item given a condition and answer to return.
     * @param conditionToCheck condition to be checked on the operation
     * @param answerToReturn answer to be returned in case of the condition is true
     * @param oClass
     */
    protected CheckConditionItem(Predicate<O> conditionToCheck, AbstractAnswer answerToReturn, Class<O> oClass) {
        this.conditionToCheck = conditionToCheck;
        this.answerToReturn = answerToReturn;
        this.oClass = oClass;
    }

    @Override
    public ProcessorConfig<O, ?> getConfig() {
        return new ProcessorConfig<O, AbstractAnswer>(oClass) {
            @Override
            public OperationProcessor<O, ? extends AbstractAnswer> initProcessor(O operation) {
                return new CheckConditionProcessor<>(operation, conditionToCheck, answerToReturn);
            }

            @Override
            public void setManager(SimCloudManager manager) {

            }
        };
    }

    @Override
    public boolean isFinal() {
        return true;
    }

    /**
     * Check that given field exists in the "query_object" field. The condition is inverse in order to return
     *  the answer in case the field does NOT exist.
     * @param queryObjectField data object field to check
     * @return a chain item to stop processing in case of the field does not exist
     */
    public static CheckConditionItem<QueryObjectOperation> checkQueryField(final String queryObjectField) {
        return new CheckConditionItem<>(
                operation -> !(operation.getQueryObject().containsField(queryObjectField)),
                AbstractAnswer.createAnswer(AbstractAnswer.QUERY_OBJ_ERROR),
                QueryObjectOperation.class);
    }

    /**
     * Check that a 'data objects' operation has not an empty list of objects. The condition is inverse in order to
     *  return the answer in case the field does NOT exist.
     * @return a chain item to stop processing in case of the field does not exist
     */
    public static <T extends ProcessObjectsOperation> CheckConditionItem<T> checkObjectsNotEmpty(Class<T> tClass) {
        return new CheckConditionItem<>(
                operation -> ! (operation.getObjectCount() > 0),
                AbstractAnswer.createAnswer(AbstractAnswer.EMPTY_ANSWER),
                tClass);
    }

    /**
     * A simple operation processor that checks a condition and returns given answer if the condition is true
     * @param <T>
     */
    static class CheckConditionProcessor<T extends AbstractOperation> extends OneStepProcessor<T, AbstractAnswer> {

        /**
         * Condition to check.
         */
        protected final Predicate<T> conditionToCheck;

        /**
         * Answer to be returned in case the condition was satisfied.
         */
        protected final AbstractAnswer answerToReturn;

        protected boolean conditionValid;

        /**
         * Creates this processor given an algorithm and operation to be executed on it.
         * @param operation operation to be executed
         * @param conditionToCheck condition to be checked
         */
        public CheckConditionProcessor(T operation, Predicate<T> conditionToCheck, AbstractAnswer answerToReturn) {
            super(operation);
            this.conditionToCheck = conditionToCheck;
            this.answerToReturn = answerToReturn;
        }

        @Override
        public AbstractAnswer finish() {
            return conditionValid ? answerToReturn : null;
        }

        @Override
        protected void process() throws AlgorithmMethodException {
            conditionValid = conditionToCheck.test(getOperation());
        }
    }
}
