/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config.impl;

import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.processing.OperationProcessor;
import messif.operation.AnnotateOperation;
import simcloud.SimCloudManager;
import simcloud.config.ProcessorConfig;

/**
 * A specific configuration that takes {@link messif.operation.AnnotateOperation} and creates a processor for this
 *  operation.
 */
public class AnnotateConfig extends ProcessorConfig<AnnotateOperation, ProcessObjectsAnswer> {

    protected SimCloudManager manager;

    public AnnotateConfig() {
        super(AnnotateOperation.class);
    }


    @Override
    public OperationProcessor<? super AnnotateOperation, ? extends ProcessObjectsAnswer> initProcessor(AnnotateOperation operation) {
        return new AnnotateOperation.Processor(operation, manager);
    }

    @Override
    public void setManager(SimCloudManager manager) {
        this.manager = manager;
    }
}
