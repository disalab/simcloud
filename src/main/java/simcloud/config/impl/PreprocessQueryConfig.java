/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config.impl;

import messif.data.DataObject;
import messif.data.processing.ObjectsProcessorEvaluator;
import messif.operation.answer.AbstractAnswer;
import messif.operation.processing.OperationProcessor;
import messif.operation.search.QueryObjectOperation;
import messif.record.processing.RecordProcessor;
import simcloud.SimCloudManager;
import simcloud.config.ProcessorConfig;
import simcloud.processor.TransformAlgProcessor;
import simcloud.transformer.impl.PreprocessQueryTransformer;

/**
 * A specific configuration that takes an operation with the query object, applies a {@link PreprocessQueryTransformer transformer}
 * and evaluates the operation on a given record processor (via {@link ObjectsProcessorEvaluator}.
 */
public class PreprocessQueryConfig extends ProcessorConfig<QueryObjectOperation, AbstractAnswer> {

    final PreprocessQueryTransformer transformer;

    final ObjectsProcessorEvaluator evaluator;

    public PreprocessQueryConfig(RecordProcessor<DataObject> recordProcessor, boolean checkFailure) {
        super(QueryObjectOperation.class);
        this.transformer = new PreprocessQueryTransformer(checkFailure);
        this.evaluator = new ObjectsProcessorEvaluator(recordProcessor, checkFailure);
    }

    @Override
    public OperationProcessor<? super QueryObjectOperation, ? extends AbstractAnswer> initProcessor(QueryObjectOperation operation) {
        return new TransformAlgProcessor<>(evaluator, operation, transformer);
    }

    @Override
    public void setManager(SimCloudManager manager) {
    }
}
