/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config.impl;

import messif.operation.crud.DeleteOperation;
import messif.operation.crud.answer.DeleteAnswer;
import messif.operation.processing.OperationProcessor;
import simcloud.SimCloudManager;
import simcloud.config.ChainItem;
import simcloud.config.ProcessorConfig;
import simcloud.processor.ChainProcessor;
import simcloud.transformer.impl.DeleteFilterTransformer;

import java.util.ArrayList;
import java.util.List;

public class DeleteConfig extends ProcessorConfig<DeleteOperation, DeleteAnswer> {

    final List<ChainItem<DeleteOperation>> chainItems;

    public DeleteConfig(String keyValueStore, String ... indexes) {
        super(DeleteOperation.class);

        chainItems = new ArrayList<>();
        chainItems.add(ChainItem.transformAlg(DeleteOperation.class, keyValueStore, new DeleteFilterTransformer(), false));
        for (String index : indexes) {
            chainItems.add(ChainItem.directAlg(DeleteOperation.class, index, false));
        }
    }

    @Override
    public OperationProcessor<DeleteOperation, DeleteAnswer> initProcessor(DeleteOperation operation) {
        return new ChainProcessor<>(operation, chainItems, new DeleteOperation.Helper(operation));
    }

    @Override
    public void setManager(SimCloudManager manager) {
        for (ChainItem<DeleteOperation> chainItem : chainItems) {
            chainItem.getConfig().setManager(manager);
        }
    }
}
