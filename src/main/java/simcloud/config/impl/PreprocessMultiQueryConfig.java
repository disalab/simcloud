/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config.impl;

import messif.data.DataObject;
import messif.data.processing.ObjectsProcessorEvaluator;
import messif.operation.answer.AbstractAnswer;
import messif.operation.processing.OperationProcessor;
import messif.operation.search.MultiQueryObjectOperation;
import messif.record.processing.RecordProcessor;
import simcloud.SimCloudManager;
import simcloud.config.ProcessorConfig;
import simcloud.processor.TransformAlgProcessor;
import simcloud.transformer.impl.GetMultiQueryTransformer;

/**
 * TODO: review if we really need three very similar configurations
 */
public class PreprocessMultiQueryConfig extends ProcessorConfig<MultiQueryObjectOperation, AbstractAnswer> {

    final GetMultiQueryTransformer transformer;

    final ObjectsProcessorEvaluator evaluator;

    public PreprocessMultiQueryConfig(RecordProcessor<DataObject> recordProcessor, boolean checkFailure) {
        super(MultiQueryObjectOperation.class);
        this.transformer = new GetMultiQueryTransformer(checkFailure);
        this.evaluator = new ObjectsProcessorEvaluator(recordProcessor, checkFailure);
    }

    @Override
    public OperationProcessor<? super MultiQueryObjectOperation, ? extends AbstractAnswer> initProcessor(MultiQueryObjectOperation operation) {
        return new TransformAlgProcessor<>(evaluator, operation, transformer);
    }

    @Override
    public void setManager(SimCloudManager manager) {
    }
}
