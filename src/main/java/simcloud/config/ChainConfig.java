/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationProcessor;
import messif.operation.processing.ProcessorExecutor;
import simcloud.SimCloudManager;
import simcloud.processor.ChainProcessor;
import simcloud.processor.ChainProcessorAsync;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A configuration that says how to process an operation by the {@link SimCloudManager}; this configuration defines
 *  a list of steps that are taken to evaluate the operation.
 * Example of such configuration is (parameters 6-10 are chain items):
 * <pre>
       kNNPreprocessor = namedInstanceConstructor
       kNNPreprocessor.param.1 = kNNPreprocessor
       kNNPreprocessor.param.2 = true
        kNNPreprocessor.param.3 = simcloud.config.ChainConfig
        kNNPreprocessor.param.4 = messif.operation.search.KNNOperation
        kNNPreprocessor.param.5 = false
        # first, try the query cache
        kNNPreprocessor.param.6 = simcloud.config.ChainItem.transformAlg(messif.operation.search.KNNOperation, queryCacheStr, simcloud.transformer.impl.KNNCacheQueryTransformer(200), true)
        # if the query object record contains ID and the ID-object storage has the object, retrieve it from the storage
        kNNPreprocessor.param.7 = simcloud.config.ChainItem.transformAlg(messif.operation.search.KNNOperation, IDObjStore, simcloud.transformer.impl.GetQueryObjectTransformer(), false)
        # use "extractor" (record processor) encapsulated as algorithm
        kNNPreprocessor.param.8 = simcloud.config.ChainItem.transformAlg(messif.operation.search.KNNOperation, httpEvaluator, simcloud.transformer.impl.DataObjectsHttpTransformer(), false)
        # add the candidate queue into the operation and execute a parallel evaluator
        kNNPreprocessor.param.9 = simcloud.config.ChainItem.transformAndConfig(messif.operation.search.KNNOperation, kNNEvaluator, simcloud.transformer.impl.AddCandidateIDQueueTransformer(messif.operation.search.KNNOperation), false)
        # store the result into the cache
        kNNPreprocessor.param.10 = simcloud.config.ChainItem.transformAlg(messif.operation.search.KNNOperation, queryCacheStr, simcloud.transformer.impl.KNNCacheStoreTransformer(), true)
 * </pre>
 */
public class ChainConfig<TOperation extends AbstractOperation, TAnswer extends AbstractAnswer>
        extends ProcessorConfig<TOperation, TAnswer> {

    private final boolean runInParallel;
    protected final List<ChainItem<TOperation>> items;

    /**
     * Executor to be used on the processor (typically either sequential or parallel).
     */
    private ProcessorExecutor executor;

    /**
     * A full constructor to set all the fields, it is typically called from a builder.
     * @param oClass      operation class to match incomming {@link AbstractOperation}
     * @param items
     */
    public ChainConfig(Class<TOperation> oClass, boolean runInParallel, ChainItem<TOperation> ... items) {
        super(new ArrayList<>(), oClass, null);
        this.runInParallel = runInParallel;
        this.items = Collections.unmodifiableList(Arrays.asList(items));
        executor =  ProcessorExecutor.SEQUENTIAL_EXECUTOR;
    }

    @Override
    public ProcessorExecutor getExecutor() {
        return executor;
    }

    @Override
    public OperationProcessor<? super TOperation, ? extends TAnswer> initProcessor(TOperation operation) {
        if (runInParallel) {
            return new ChainProcessorAsync<>(operation, items);
        }
        return new ChainProcessor<>(operation, items, null);
    }

    public void setManager(SimCloudManager manager) {
        for (ChainItem<TOperation> item : items) {
            item.getConfig().setManager(manager);
        }
        if (runInParallel) {
            executor = new ProcessorExecutor.ParallelExecutor(manager.getThreadPool());
        }
    }
}
