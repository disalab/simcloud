/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationEvaluator;
import simcloud.SimCloudManager;

import java.util.Map;

/**
 * Created by david on 01/12/16.
 */
public abstract class OnAlgorithmConfig<TOperation extends AbstractOperation, TAnswer extends AbstractAnswer>
        extends ProcessorConfig<TOperation, TAnswer> {


    protected Map<String, OperationEvaluator> algorithms;

    /**
     * Name of the algorithm within the {@link SimCloudManager}.
     */
    protected final String algorithmName;

    /**
     * A full constructor to set all the fields, it is typically called from a builder.
     *
     * @param oClass      operation class to match incomming {@link AbstractOperation}
     */
    public OnAlgorithmConfig(String algorithmName, Class<TOperation> oClass) {
        super(oClass);
        this.algorithmName = algorithmName;
    }

    public void setManager(SimCloudManager manager) {
        this.algorithms = manager.getNamedAlgorithms();
    }

    /**
     * Returns the actual algorithm corresponding to the given <code>algorithmName</code> under current
     *  context (actual cloud manager).
     * @return actual algorithm for the algorithm name, or null if it does not exist
     */
    protected OperationEvaluator getAlgorithm(String algorithmName) {
        return algorithms.get(algorithmName);
    }

}
