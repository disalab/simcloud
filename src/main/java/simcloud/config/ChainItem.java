/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config;


import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationProcessor;
import simcloud.SimCloudManager;
import simcloud.transformer.OperationTransformer;
import simcloud.transformer.OperationTransformerChain;

/**
 * An interface for all items of a chain of operations -- see {@link ChainConfig} and {@link simcloud.processor.ChainProcessor}.
 * Example of such configuration is (parameters 6-10 are chain items):
 * <pre>
    kNNPreprocessor = namedInstanceConstructor
    kNNPreprocessor.param.1 = kNNPreprocessor
    kNNPreprocessor.param.2 = true
    kNNPreprocessor.param.3 = simcloud.config.ChainConfig
    kNNPreprocessor.param.4 = messif.operation.search.KNNOperation
    kNNPreprocessor.param.5 = false
    # first, try the query cache
    kNNPreprocessor.param.6 = simcloud.config.ChainItem.transformAlg(messif.operation.search.KNNOperation, queryCacheStr, simcloud.transformer.impl.KNNCacheQueryTransformer(200), true)
    # if the query object record contains ID and the ID-object storage has the object, retrieve it from the storage
    kNNPreprocessor.param.7 = simcloud.config.ChainItem.transformAlg(messif.operation.search.KNNOperation, IDObjStore, simcloud.transformer.impl.GetQueryObjectTransformer(), false)
    # use "extractor" (record processor) encapsulated as algorithm
    kNNPreprocessor.param.8 = simcloud.config.ChainItem.transformAlg(messif.operation.search.KNNOperation, httpEvaluator, simcloud.transformer.impl.DataObjectsHttpTransformer(), false)
    # add the candidate queue into the operation and execute a parallel evaluator
    kNNPreprocessor.param.9 = simcloud.config.ChainItem.transformAndConfig(messif.operation.search.KNNOperation, kNNEvaluator, simcloud.transformer.impl.AddCandidateIDQueueTransformer(messif.operation.search.KNNOperation), false)
    # store the result into the cache
    kNNPreprocessor.param.10 = simcloud.config.ChainItem.transformAlg(messif.operation.search.KNNOperation, queryCacheStr, simcloud.transformer.impl.KNNCacheStoreTransformer(), true)
 * </pre>
 */
public interface ChainItem<TOperation extends AbstractOperation> {

    /**
     * Get the actual operation processing configuration for given step.
     * @return
     */
    ProcessorConfig<TOperation, ?> getConfig();

    /**
     * If this step is successful AND this step is marked by this method to be FINAL, the processing stops prematurely.
     * @return true/false if this step can stop prematurely
     */
    boolean isFinal();


    // region **********************       Static factory methods         *****************************

    /**
     * Process directly on a given {@link messif.algorithm.Algorithm} or {@link messif.operation.processing.OperationEvaluator}
     *  without any transformations.
     * @param oClass
     * @param algorithmName
     * @param isFinal
     * @param <TOperation>
     * @return
     */
    static <TOperation extends AbstractOperation> ChainItem<TOperation>
            directAlg(final Class<TOperation> oClass, final String algorithmName, final boolean isFinal) {
        return new ChainItem<TOperation>() {

            final ProcessorConfig<TOperation, ?> config = ProcessorConfig.directAlg(oClass, algorithmName);

            @Override
            public ProcessorConfig<TOperation, ?> getConfig() {
                return config;
            }

            @Override
            public boolean isFinal() {
                return isFinal;
            }
        };
    }

    static <TOperation extends AbstractOperation> ChainItem<TOperation>
        transformAlg(final Class<TOperation> oClass, final String algorithmName, final OperationTransformerChain<TOperation, ?, ?> transformer, boolean isFinal) {
        return new ChainItem<TOperation>() {

            final ProcessorConfig<TOperation, ?> config = ProcessorConfig.transformAlg(oClass, algorithmName, transformer);

            @Override
            public ProcessorConfig<TOperation, ?> getConfig() {
                return config;
            }

            @Override
            public boolean isFinal() {
                return isFinal;
            }
        };
    }

    /**
     * Just pass the processing to a given processor configuration.
     * @param config
     * @param isFinal
     * @param <TOperation>
     * @return
     */
    static <TOperation extends AbstractOperation> ChainItem<TOperation>
        givenConfig(final ProcessorConfig<TOperation, ?> config, boolean isFinal) {
        return new ChainItem<TOperation>() {

            @Override
            public ProcessorConfig<TOperation, ?> getConfig() {
                return config;
            }

            @Override
            public boolean isFinal() {
                return isFinal;
            }
        };
    }

    static <O extends AbstractOperation, T extends AbstractOperation, P extends AbstractAnswer, A extends AbstractAnswer> ChainItem<O>
        transformAndConfig(final Class<O> oClass, final ProcessorConfig<T, P> config, final OperationTransformerChain<O, T, A> transformer, boolean isFinal) {
        return new ChainItem<O>() {
            @Override
            public ProcessorConfig<O, ?> getConfig() {
                return ProcessorConfig.transformAndConfig(oClass, config, transformer);
            }

            @Override
            public boolean isFinal() {
                return isFinal;
            }
        };
    }

    /**
     * A dummy chain item that only transforms the operation (does not change the type) and does no processing.
     * @param oClass class of the operation to be transformed (and
     * @param transformer
     * @param <O>
     * @return
     */
    static <O extends AbstractOperation> ChainItem<O>
            transformOperation(final Class<O> oClass, final OperationTransformer<O, O> transformer) {
        return new ChainItem<O>() {
            @Override
            public ProcessorConfig<O, AbstractAnswer> getConfig() {
                return new ProcessorConfig<O, AbstractAnswer>(oClass) {
                    @Override
                    public OperationProcessor<O, AbstractAnswer> initProcessor(final O operation) {
                        return new OperationProcessor<O,AbstractAnswer>() {
                            @Override
                            public O getOperation() {
                                return transformer.transformOperation(operation);
                            }
                            @Override
                            public boolean processStep() throws Exception {
                                return false;
                            }
                            @Override
                            public AbstractAnswer finish() {
                                return null;
                            }
                        };
                    }
                    @Override
                    public void setManager(SimCloudManager manager) { }
                };
            }

            @Override
            public boolean isFinal() {
                return false;
            }
        };
    }

    // endregion

}
