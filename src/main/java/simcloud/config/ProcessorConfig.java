/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.config;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationProcessor;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.ProcessorExecutor;
import messif.record.Record;
import simcloud.SimCloudManager;
import simcloud.processor.DirectAlgProcessor;
import simcloud.processor.TransformAlgProcessor;
import simcloud.processor.TransformEncapsulateProcessor;
import simcloud.transformer.OperationTransformerChain;

import java.util.ArrayList;
import java.util.Collection;

/**
 * An extension of the standard {@link ProcessorConfiguration} that says which processor should be used for which
 *  operation type.
 *
 * Instances of this class exist during the whole live cycle of a SimSearchManager.
 */
public abstract class ProcessorConfig<TOperation extends AbstractOperation, TAnswer extends AbstractAnswer>
        extends ProcessorConfiguration<TOperation, TAnswer> {

    public ProcessorConfig(Class<TOperation> oClass) {
        this(new ArrayList<>(), oClass, ProcessorExecutor.SEQUENTIAL_EXECUTOR);
    }

    /**
     * A full constructor to set all the fields, it is typically called from a builder.
     *
     * @param typeStrings a list of type strings to match {@link Record}.
     * @param oClass      operation class to match incomming {@link AbstractOperation}
     * @param executor    a seq/parallel executor to be used with this processor
     */
    public ProcessorConfig(Collection<String> typeStrings, Class<TOperation> oClass, ProcessorExecutor executor) {
        super(typeStrings, oClass, null, executor);
    }

    public abstract OperationProcessor<? super TOperation, ? extends TAnswer> initProcessor(TOperation operation);

    public abstract void setManager(SimCloudManager manager);


    // region ******************       Static factory methods

    /**
     * Create an operation processor configuration that executes the operation directly on an algorithm, without
     *  any modifications.
     * @param oClass class of the operation to match this processor
     * @param algorithmName name of the algorithm to process the operation by
     * @return a configuration that executes the operation on the specified algorithm
     */
    public static <TOperation extends AbstractOperation, TAnswer extends AbstractAnswer> ProcessorConfig<TOperation, TAnswer>
            directAlg(final Class<TOperation> oClass, final String algorithmName) {
        return new OnAlgorithmConfig<TOperation, TAnswer>(algorithmName, oClass) {
            public OperationProcessor<? super TOperation, ? extends TAnswer> initProcessor(TOperation operation) {
                return new DirectAlgProcessor<>(getAlgorithm(algorithmName), operation);
            }
        };
    }

    public static <TOperation extends AbstractOperation, TAnswer extends AbstractAnswer> ProcessorConfig<TOperation, TAnswer>
            transformAlg(final Class<TOperation> oClass, final String algorithmName, final OperationTransformerChain<TOperation, ?, TAnswer> transformer) {
        return new OnAlgorithmConfig<TOperation, TAnswer>(algorithmName, oClass) {
            public OperationProcessor<? super TOperation, ? extends TAnswer> initProcessor(TOperation operation) {
                return new TransformAlgProcessor(getAlgorithm(algorithmName), operation, transformer);
            }
        };
    }

    public static <O extends AbstractOperation, T extends AbstractOperation, P extends AbstractAnswer, A extends AbstractAnswer> ProcessorConfig<O, A>
        transformAndConfig(final Class<O> oClass, final ProcessorConfig<T, P> innerConfig, final OperationTransformerChain<O, T, A> transformer) {
        return new ProcessorConfig<O, A>(new ArrayList<>(), oClass, ProcessorExecutor.SEQUENTIAL_EXECUTOR) {
            @Override
            public OperationProcessor<? super O, ? extends A> initProcessor(O operation) {
                return new TransformEncapsulateProcessor<>(innerConfig, operation, transformer);
            }

            @Override
            public void setManager(SimCloudManager manager) {
                innerConfig.setManager(manager);
            }
        };
    }

    // endregion

}
