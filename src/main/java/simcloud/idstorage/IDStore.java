/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.idstorage;


import messif.bucket.BucketStorageException;
import messif.data.DataObject;

import java.util.Collection;
import java.util.Iterator;

/**
 * Interface for storing data objects based on their string locator, reading them and 
 *  refining a candidate set of locator-specified objects for a given search query.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 * @param <T> type of data stored in this storage/refiner
 */
public interface IDStore<T extends DataObject>  {
    
    
   /**
     * Given a locator ID, this method returns the stored object.
     * @param id string identifier of the object to be returned
     * @return stored object identified by the identifier passed
     */
   T readObject(String id);

    /**
     * Given a list of String IDs, this method returns the corresponding stored objects. The
     *  returned objects does not have to be in the same order! The objects that were not found
     *  are simply not returned by the iterator.
     * @param ids string identifiers of the objects to be returned
     * @return iterator over the object identified by the identifier passed
     */
    Iterator<T> readObjects(Collection<String> ids);

    /**
     * Given a list of String IDs, this method checks which of these IDs are stored in the storage.
     * @param ids string identifiers of the objects to be checked
     * @return a collection of IDs that are stored in this {@link IDStore} (can be in different order)
     */
    Collection<String> checkIDs(Collection<String> ids);

    /**
     * Given a list of String IDs, this method checks which of these IDs are stored in the storage.
     * @param id a string identifier to be checked
     * @return a collection of IDs that are stored in this {@link IDStore} (can be in different order)
     */
    boolean checkID(String id);

    /**
     * Stores given object to this storage but only if object with such ID was not stored.
     * @param id string locator ID to store this object by
     * @param object object to be stored
     * @return true if the object was stored, false otherwise (most probably because such ID-object already exists)
     * @throws BucketStorageException if the storage fails
     */
    boolean storeObject(String id, T object) throws BucketStorageException;

    /**
     * Deletes object identified by given ID.
     * @param id ID of the object to be deleted.
     * @throws BucketStorageException if the storage fails
     * @return the deleted object or NULL if it was not stored.
     */
    T deleteObject(String id) throws BucketStorageException;
    
    /**
     * Get number of objects in the store.
     * @return number of objects in the store
     */
    int getNumberOfObjects();
    
}
