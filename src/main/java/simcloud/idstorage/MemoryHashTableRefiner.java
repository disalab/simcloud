/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.idstorage;

import messif.bucket.BucketStorageException;
import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.data.util.DataObjectList;
import messif.distance.DistanceFunc;
import messif.operation.params.CandidateIDQueue;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.search.GetRandomObjectsOperation;
import messif.operation.search.QueryObjectOperation;
import messif.operation.answer.ListingAnswer;
import messif.operation.answer.RankedAnswer;
import messif.utility.ExtendedProperties;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A simple implementation of the {@link StorageAndRefinerAlgorithm} that keeps the data in a memory {@link HashMap}.
 *
 * @author xnovak8
 */
public class MemoryHashTableRefiner extends StorageAndRefinerAlgorithm implements Serializable {

    /**
     * Class serial id for serialization.
     */
    private static final long serialVersionUID = 8593102L;

    /**
     * Memory hash map of the data objects indexed by their string locators.
     */
    private final Map<String, DataObject> storage = new HashMap<>();

    @AlgorithmConstructor(description = "Creates new memory map refiner", arguments = {})
    public MemoryHashTableRefiner(DistanceFunc<DataObject> defaultDistanceFunc, ExtendedProperties configuration) throws IllegalArgumentException {
        super("memory ID-object storage and refiner", defaultDistanceFunc, configuration);
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();
        // refine the provided candidates OR run exhaustive search (sequential scan)
        this.handler.register(ProcessorConfiguration.create(QueryObjectOperation.class, RankedAnswer.class)
                .withProcessor((op) -> {
                    if (op instanceof CandidateIDQueue)
                        return new RefineProcessor(op, (CandidateIDQueue) op, defaultDistanceFunc);
                    return new SeqScanProcessor(op);
                })
                .executeParallel(threadPool).build());

        this.handler.register(ProcessorConfiguration.create(GetRandomObjectsOperation.class, ListingAnswer.class)
                .withProcessor(operation -> new GetRandomProcessor(operation))
                .executeSequential().build());

    }

    @Override
    public DataObject readObject(String id) {
        return storage.get(id);
    }

    @Override
    public DataObjectIterator readObjects(Collection<String> ids) {
        DataObjectList retVal = new DataObjectList(ids.size());
        for (String string : ids) {
            DataObject obj = storage.get(string);
            if (obj != null) {
                retVal.add(obj);
            }
        }
        return retVal.iterator();
    }

    @Override
    public boolean checkID(String id) {
        return storage.containsKey(id);
    }

    @Override
    public Collection<String> checkIDs(Collection<String> ids) {
        return ids.parallelStream().filter(storage::containsKey).collect(Collectors.toList());
    }

    @Override
    public boolean storeObject(String id, DataObject object) throws BucketStorageException {
        if (storage.containsKey(id)) {
            return false;
        }
        storage.put(id, object);
        return true;
    }

    @Override
    public DataObject deleteObject(String id) {
        return storage.remove(id);
    }

    @Override
    public int getNumberOfObjects() {
        return storage.size();
    }


    protected class SeqScanProcessor extends OneStepProcessor<QueryObjectOperation, RankedAnswer> {

        final QueryObjectOperation.Helper helper;

        public SeqScanProcessor(QueryObjectOperation operation) {
            super(operation);
            helper = new QueryObjectOperation.Helper(operation, defaultDistanceFunc);
        }

        @Override
        protected void process() throws Exception {
            helper.evaluate(storage.values().iterator());
        }

        @Override
        public RankedAnswer finish() {
            return helper.getAnswer();
        }
    }

    protected class GetRandomProcessor extends OneStepProcessor<GetRandomObjectsOperation, ListingAnswer> {

        final GetRandomObjectsOperation.Helper helper;

        public GetRandomProcessor(GetRandomObjectsOperation operation) {
            super(operation);
            this.helper = new GetRandomObjectsOperation.Helper(operation);
        }

        @Override
        protected void process() throws Exception {
            helper.evaluate(DataObjectIterator.getIterator(storage.values().iterator()));
        }

        @Override
        public ListingAnswer finish() {
            return helper.getAnswer();
        }
    }

    @Override
    public void destroy() throws Throwable {
        super.destroy();
    }

    @Override
    @SuppressWarnings("FinalizeNotProtected")
    public void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }
}
