/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.idstorage;


import gnu.trove.impl.Constants;
import gnu.trove.iterator.TLongIterator;
import gnu.trove.map.hash.TObjectLongHashMap;
import messif.algorithm.Algorithm;
import messif.bucket.BucketStorageException;
import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.distance.DistanceFunc;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.search.GetRandomObjectsOperation;
import messif.operation.answer.ListingAnswer;
import messif.storage.LongStorageSearch;
import messif.storage.impl.DiskStorage;
import messif.utility.ExtendedProperties;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static messif.data.util.DataObjectIterator.getIterator;

/**
 * This class keeps a disk storage of data objects and a memory locator-position index to access the objects directly. 
 *  It is not serializable but assumed to be created from given storage (and from a serialized index).
 * 
 * @author xnovak8
 */
public class DiskStorageHashRefiner extends StorageAndRefinerAlgorithm {
    
    /** Special value that indicates that the index does not contain given key. */
    public static final long NO_ENTRY_VALUE = Long.MIN_VALUE;
    
    /** Hash index on object locators (IDs). */
    private final TObjectLongHashMap<String> index;
    
    /** Storage associated with this index. */
    private final DiskStorage<DataObject> storage;

    /** Random number generator. */
    private final static Random random = new Random(System.currentTimeMillis());
        
    /**
     * Creates the object from given storage - either empty or not. The hash index is created by sequential scanning
     *  the storage.
     * @param storage existing disk storage on data objects
     */
    public DiskStorageHashRefiner(DiskStorage<DataObject> storage, DistanceFunc<DataObject> defaultDistanceFunc) {
        this(storage, null, defaultDistanceFunc, Algorithm.defaultConfiguration());
    }
    
    /**
     * Creates the object from given storage - either empty or not. The hash index is read from the specified serialized 
     *  file (or created by sequential scanning the storage, if the stored index cannot be read).
     * @param storage existing disk storage on data objects
     * @param serializedIndex path to file where the index to given storage is stored
     */
    public DiskStorageHashRefiner(DiskStorage<DataObject> storage, String serializedIndex, DistanceFunc<DataObject> defaultDistanceFunc, ExtendedProperties configuration) {
        super("simple disk ID-object storage algorithm", defaultDistanceFunc, configuration);
        this.storage = storage;
        TObjectLongHashMap<String> createdIndex = null;
        if (new File(serializedIndex).canRead()) {
            try (ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(serializedIndex)))) {
                createdIndex = (TObjectLongHashMap) in.readObject();
            } catch (ClassNotFoundException | IOException ex) {
                Logger.getLogger(DiskStorageHashRefiner.class.getName()).log(Level.WARNING, null, ex);
            }
        }
        if (createdIndex == null) {
            createdIndex = new TObjectLongHashMap(Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, NO_ENTRY_VALUE);
            fillIndexFromStorage(createdIndex);
        }
        this.index = createdIndex;
    }
        
    /**
     * Creates hash index by scanning the whole storage.
     */
    private void fillIndexFromStorage(TObjectLongHashMap<String> createdIndex) {
        LongStorageSearch<DataObject> allObjs = storage.search();
        while (allObjs.next()) {
            createdIndex.put(allObjs.getCurrentObject().getID(), allObjs.getCurrentObjectLongAddress());
        }
    }
    
    /**
     * Public API method to store the internal index to given file.
     * @param fileName name of the file to store the index to
     * @throws IOException if the store fails
     */
    public void storeIndexToFile(String fileName) throws IOException {
        try (ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))) {
            out.writeObject(this.index);
        }        
    }
    
    @Override
    public DataObject readObject(String id) {
        try {
            return storage.read(index.get(id));
        } catch (BucketStorageException ex) {
            Logger.getLogger(DiskStorageHashRefiner.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public DataObjectIterator readObjects(Collection<String> ids) {
        final long [] positions = new long [ids.size()];
        int i = 0;
        for (String id : ids) {
            long position = index.get(id);
            if (position != NO_ENTRY_VALUE) {
                positions[i ++] = position;            
            }
        }
        if (i == ids.size()) {
            return getIterator(storage.read(positions));
        } else {
            return getIterator(storage.read(Arrays.copyOf(positions, i)));
        }
    }

    @Override
    public boolean checkID(String id) {
        return index.containsKey(id);
    }

    @Override
    public Collection<String> checkIDs(Collection<String> ids) {
        return ids.parallelStream().filter(index::containsKey).collect(Collectors.toList());
    }

    @Override
    public boolean storeObject(String id, DataObject object) throws BucketStorageException {
        if (index.containsKey(id)) {
            return false;
        }
        index.put(id, storage.store(object).getAddress());
        return true;
    }

    @Override
    public DataObject deleteObject(String id) throws BucketStorageException {
        DataObject retVal = null;
        if (index.containsKey(id)) {
            retVal = storage.read(index.get(id));
            storage.remove(index.remove(id));
        }
        return retVal;
    }

    @Override
    public int getNumberOfObjects() {
        return storage.size();
    }

    protected class GetRandomProcessor extends OneStepProcessor<GetRandomObjectsOperation, ListingAnswer> {

        final GetRandomObjectsOperation.Helper helper;

        public GetRandomProcessor(GetRandomObjectsOperation operation) {
            super(operation);
            this.helper = new GetRandomObjectsOperation.Helper(operation);
        }

        @Override
        protected void process() throws Exception {
            int number = operation.getObjectCount();
            if (index.isEmpty() || number <= 0) {
                return;
            }

            TLongIterator iterator = index.valueCollection().iterator();
            for (int i = 0; i < number; i++) {
                int skips = random.nextInt(index.size() / number);

                int counter = 0;
                long retValAddress;
                do {
                    retValAddress = iterator.next();
                } while (iterator.hasNext() && counter++ < skips);
                helper.add(storage.read(retValAddress));
            }
        }

        @Override
        public ListingAnswer finish() {
            return helper.getAnswer();
        }
    }

    @Override
    public void destroy() throws Throwable {
        storage.destroy();
        super.destroy();
    }

    @Override
    @SuppressWarnings("FinalizeNotProtected")
    public void finalize() throws Throwable {
        storage.finalize();
        super.finalize();
    }
}
