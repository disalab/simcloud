/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.idstorage;


import messif.algorithm.Algorithm;
import messif.bucket.BucketStorageException;
import messif.data.DataObject;
import messif.data.util.DataObjectList;
import messif.distance.DistanceFunc;
import messif.operation.answer.JoinQueryAnswer;
import messif.operation.answer.RankedAnswer;
import messif.operation.crud.*;
import messif.operation.crud.answer.*;
import messif.operation.info.ObjectCountOperation;
import messif.operation.info.answer.ObjectCountAnswer;
import messif.operation.params.CandidateIDQueue;
import messif.operation.params.CandidateIDSetQueue;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.processing.impl.RefineJoinProcessorBase;
import messif.operation.processing.impl.RefineProcessorBase;
import messif.operation.search.JoinOperation;
import messif.operation.search.MultiQueryObjectOperation;
import messif.operation.search.QueryObjectOperation;
import messif.operation.search.RankingOperation;
import messif.record.Record;
import messif.utility.ExtendedProperties;
import simcloud.filter.DataObjectCondition;
import simcloud.filter.SelectionCondition;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;

/**
 * This is an abstract class implementing partially the {@link IDStore} interface and, especially, 
 *  extending Algorithm. It is to be extended by algorithms that cover specific ID-object storages.
 * 
 * @author xnovak8
 */
public abstract class StorageAndRefinerAlgorithm extends Algorithm implements IDStore<DataObject> {

    protected final DistanceFunc<DataObject> defaultDistanceFunc;

    /**
     * Create the algorithm given a name.
     * @param algorithmName name of the algorithm
     * @throws IllegalArgumentException
     */
    public StorageAndRefinerAlgorithm(String algorithmName, DistanceFunc<DataObject> defaultDistanceFunc, ExtendedProperties configuration) throws IllegalArgumentException {
        super(algorithmName, configuration);
        this.defaultDistanceFunc = defaultDistanceFunc;
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();
        this.handler.register(ProcessorConfiguration.create(InsertOperation.class, InsertAnswer.class)
                .withProcessor(operation -> new InsertProcessor(operation))
                .build());
        this.handler.register(ProcessorConfiguration.create(DeleteOperation.class, DeleteAnswer.class)
                .withProcessor(operation -> new DeleteProcessor(operation))
                .build());
        this.handler.register(ProcessorConfiguration.create(UpdateOperation.class, UpdateAnswer.class)
                .withProcessor(operation -> new UpdateProcessor(operation))
                .build());
        this.handler.register(ProcessorConfiguration.create(GetObjectsOperation.class, GetObjectsAnswer.class)
                .withProcessor((op) -> {
                    if (Arrays.deepEquals(ModifyingOperation.DEFAULT_LIST,op.fieldsToReturn())) {
                        return new CheckObjectsProcessor(op);
                    } else {
                        return new GetObjectsProcessor(op);
                    }
                })
                .build());

        this.handler.register(ProcessorConfiguration.create(ObjectCountOperation.class, ObjectCountAnswer.class)
                .withProcessor(operation -> new GetCountProcessor(operation))
                .executeSequential().build());

        // refine the provided candidates
        this.handler.register(ProcessorConfiguration.create(QueryObjectOperation.class, RankedAnswer.class)
                .withProcessor((op) -> {
                    if (op instanceof CandidateIDQueue)
                        return new RefineProcessor(op, (CandidateIDQueue) op, defaultDistanceFunc);
                    return null;
                })
                .executeParallel(threadPool).build());
        // refine the provided candidates
        this.handler.register(ProcessorConfiguration.create(MultiQueryObjectOperation.class, RankedAnswer.class)
                .withProcessor((op) -> {
                    if (op instanceof CandidateIDQueue)
                        return new RefineProcessor(op, (CandidateIDQueue) op, defaultDistanceFunc);
                    return null;
                })
                .executeParallel(threadPool).build());

        this.handler.register(ProcessorConfiguration.create(JoinOperation.class, JoinQueryAnswer.class)
                .withProcessor((op) -> {
                    if (op instanceof CandidateIDSetQueue)
                        return new JoinRefineProcessor(op, (CandidateIDSetQueue) op, defaultDistanceFunc);
                    return null;
                })
                .executeParallel(threadPool).build());
    }

    /**
     * Processor base for the insert/update/delete/get operation.
     */
    protected abstract class CRUDProcessor<O extends CRUDOperation, A extends CRUDAnswer> extends OneStepProcessor<O, A> {

        protected final CRUDOperation.Helper<O,A> helper;

        public CRUDProcessor(O operation, CRUDOperation.Helper<O,A> helper) {
            super(operation);
            this.helper = helper;
        }

        @Override
        public A finish() {
            return helper.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            try {
                for (DataObject dataObject : operation.getObjects()) {
                    processObject(dataObject);
                }
            } catch (BucketStorageException ex) {
                helper.errorCodeHelper.updateErrorCode(ex.getErrorCode());
            }
        }

        protected abstract void processObject(DataObject dataObject) throws BucketStorageException;
    }

    protected class InsertProcessor extends CRUDProcessor<InsertOperation, InsertAnswer> {

        public InsertProcessor(InsertOperation operation) {
            super(operation, new InsertOperation.Helper(operation));
        }

        @Override
        protected void processObject(DataObject dataObject) throws BucketStorageException {
            helper.objectProcessed(dataObject, storeObject(dataObject.getID(), dataObject) ?
                    InsertAnswer.OBJECT_INSERTED : InsertAnswer.OBJECT_DUPLICATE);
        }
    }

    protected class DeleteProcessor extends CRUDProcessor<DeleteOperation, DeleteAnswer> {

        public DeleteProcessor(DeleteOperation operation) {
            super(operation, new DeleteOperation.Helper(operation));
        }

        @Override
        protected void processObject(DataObject dataObject) throws BucketStorageException {
            DataObject deleted = deleteObject(dataObject.getID());
            if (deleted != null) {
                helper.objectProcessed(deleted, DeleteAnswer.OBJECT_DELETED);
            } else {
                helper.objectProcessed(dataObject, CRUDAnswer.OBJECT_NOT_FOUND);
            }
        }
    }

    protected class UpdateProcessor extends CRUDProcessor<UpdateOperation, UpdateAnswer> {

        public UpdateProcessor(UpdateOperation operation) {
            super(operation, new UpdateOperation.Helper(operation));
        }

        @Override
        protected void processObject(DataObject dataObject) throws BucketStorageException {
            final DataObject originalObject = deleteObject(dataObject.getID());
            if (originalObject != null) {
                DataObject updatedObject = ((UpdateOperation.Helper) helper).updateOneObject(originalObject, dataObject);
                storeObject(dataObject.getID(), updatedObject);
                helper.objectProcessed(updatedObject, CRUDAnswer.OBJECT_FOUND);
            } else {
                helper.objectProcessed(dataObject, CRUDAnswer.OBJECT_NOT_FOUND);
            }
        }
    }

    protected class GetObjectsProcessor extends CRUDProcessor<GetObjectsOperation, GetObjectsAnswer> {

        public GetObjectsProcessor(GetObjectsOperation operation) {
            super(operation, new GetObjectsOperation.Helper(operation));
        }

        @Override
        protected void processObject(DataObject dataObject) throws BucketStorageException {
            final DataObject object = readObject(dataObject.getID());
            if (object != null) {
                helper.objectProcessed(object, CRUDAnswer.OBJECT_FOUND);
            } else {
                helper.objectProcessed(dataObject, CRUDAnswer.OBJECT_NOT_FOUND);
            }
        }
    }

    protected class CheckObjectsProcessor extends GetObjectsProcessor {

        public CheckObjectsProcessor(GetObjectsOperation operation) {
            super(operation);
        }

        @Override
        protected void processObject(DataObject dataObject) throws BucketStorageException {
            helper.objectProcessed(dataObject, checkID(dataObject.getID()) ? CRUDAnswer.OBJECT_FOUND : CRUDAnswer.OBJECT_NOT_FOUND);
        }
    }

    protected class GetCountProcessor extends OneStepProcessor<ObjectCountOperation, ObjectCountAnswer> {

        final ObjectCountOperation.Helper helper;

        /**
         * Creates this processor given the operation to be processed.
         *
         * @param operation operation to be executed
         */
        public GetCountProcessor(ObjectCountOperation operation) {
            super(operation);
            this.helper = new ObjectCountOperation.Helper();
        }

        @Override
        protected void process() throws Exception {
            helper.addToAnswer(getNumberOfObjects());
        }

        @Override
        public ObjectCountAnswer finish() {
            return helper.getAnswer();
        }
    }

    /**
     * Creates a processor for refinement of any approximate (single)-query operation. It applies the filter.
     */
    protected class RefineProcessor extends RefineProcessorBase {

        final SelectionCondition<DataObject> filterCondition;

        /**
         * Creates this processor given a query operation and a distance to be used if the query does not contain
         * a distance itself.
         *
         * @param query
         * @param defaultDistanceFunc
         */
        public RefineProcessor(RankingOperation query, CandidateIDQueue candidateIDQueue, DistanceFunc<DataObject> defaultDistanceFunc) {
            super(query, candidateIDQueue, defaultDistanceFunc);
            if (query.getParams().containsField(DataObjectCondition.QUERY_FILTER_PARAM)) {
                filterCondition = DataObjectCondition.valueOf(query.getParams().getField(DataObjectCondition.QUERY_FILTER_PARAM, Record.class));
            } else {
                filterCondition = null;
            }
        }

        @Override
        protected DataObject getObject(@Nonnull String id) {
            final DataObject dataObject = readObject(id);
            if (filterCondition == null || dataObject == null || filterCondition.matches(dataObject)) {
                return dataObject;
            }
            return null;
        }
    }

    /**
     * Refine a self-join similarity operation.
     */
    protected class JoinRefineProcessor extends RefineJoinProcessorBase {

        /**
         * Creates this processor given a query operation and a distance to be used if the query does not contain
         * a distance itself.
         *
         * @param operation a self-join operation
         * @param candidateIDSetQueue queue with sets of "IDs" to be refined
         * @param defaultDistanceFunc a default distance function be used in case the function is not part of the operation
         */
        public JoinRefineProcessor(JoinOperation operation, CandidateIDSetQueue candidateIDSetQueue, DistanceFunc<DataObject> defaultDistanceFunc) {
            super(operation, candidateIDSetQueue, defaultDistanceFunc);
        }

        @Override
        protected List<DataObject> getObjects(List<String> ids) {
            return new DataObjectList(readObjects(ids));
        }
    }

}
