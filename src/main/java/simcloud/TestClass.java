/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud;

/**
 * Created by david on 01/12/16.
 */
public class TestClass {

    protected static void calculateInfl(float [] distances, float influenceOfDistance) {
        //float maxDistance = distances[distances.length - 1] * (1f/influenceOfDistance);
        float maxDistance = 100f * (1f/influenceOfDistance);
        for (float distance : distances) {
            //float influence = (maxDistance - distance) / maxDistance;
            float influence = Math.max(0f, (maxDistance - distance) / maxDistance);
            System.out.println("distance: " + distance + ", influence: " + influence);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        float[] distances = new float[]{5.0f, 26.0f, 45.0f, 50f, 50.1f, 50.3f, 55f};
        float influenceOfDistance = 1f;

        calculateInfl(distances, influenceOfDistance);

        distances = new float[]{10.0f, 52.0f, 90.0f, 100f, 100.2f, 100.6f, 110f};
        calculateInfl(distances, influenceOfDistance);
    }

}
