/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package simcloud.filter;

import junit.framework.TestCase;
import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.data.util.StreamDataObjectIterator;
import messif.record.Record;
import messif.utility.json.JSONReader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

/**
 *
 * @author david
 */
public class ConditionTest extends TestCase {
    
    public ConditionTest(String testName) {
        super(testName);
    }


    public static DataObjectIterator getDataObjects() {
        return new StreamDataObjectIterator(new BufferedReader(
                new InputStreamReader(ConditionTest.class.getResourceAsStream("/dataobjects.json"))));
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

//    private final static String keywordString1 = " , ONE:1:Jedna,dva:2:two:TWO, ";
//    private final static String keywordStringOne = "one:1";
//    private final static String keywordStringTwo = "TWO:dva";
//    private final static String keywordStringOneAndTwo = "one:1,TWO:2";
//
//    private final static String keywordsData1 = "one,1,zero,sdf,sdfadf,XXXXX ";
//    private final static String keywordsData2 = ",zero,sdf,sdfadf,XXXXX ,two";
//    private final static String keywordsData12 = "one,1,two,three";

    public static final String integerCondition = "{ \"test_int\": 2 }";
    public static final String integerConditionLT = "{ \"test_int\": { \"$lte\": 2 } }";
    public static final String integerConditionNE = "{ \"test_int\": { \"$ne\": 2 } }";
    public static final String integerConditionGT = "{ \"test_int\": { \"$gt\": 2 } }";

    public void testIntegers() {
        testCondition(integerCondition);
        testCondition(integerConditionLT);
        testCondition(integerConditionNE);
        testCondition(integerConditionGT);

        testCondition("{ \"name\": \"honza\" }");
        testCondition("{ \"name\": { \"$ne\": \"honza\" } }");

        testCondition("{ \"test data\": 1.0 }");
        testCondition("{ \"test data\": 2.0, \"name\": \"honza\" }");
        testCondition("{ \"$and\": [ { \"test data\": 2.0 }, { \"test data\": 2.1 } ] }");

        testCondition("{ \"structure\": { \"next\": \"string2\" } }");
        testCondition("{ \"name\": { \"$in\": [ \"martin\", \"david\" ] } }");

        // conditions that goes deep than the data
        testCondition("{ \"structure\": { \"next\": { \"next next\": \"string2\" } } }");
        testCondition("{ \"structure\": { \"next\": { \"$gt\": 3 } } }");
        testCondition("{ \"structure\": { \"next\": { \"$in\": [ 3, 4 ] } } }");
        testCondition("{ \"structure\": { \"next\": { \"next next\": { \"$eq\": 3 } } } }");

        testCondition("{ \"structure\": { \"$not\": { \"next\": { \"$eq\": [\"string\",\"next string\"] } } } }");
        testCondition("{ \"structure\": { \"next\": { \"$not\": { \"next next\": { \"$eq\": 3 } } } } }");
    }

    public void testCondition(String conditionString) {
        DataObjectIterator dataObjectIterator = getDataObjects();
        final Record integerTest = (Record) JSONReader.readObjectFrom(conditionString, true);
        final SelectionCondition<DataObject> condition = DataObjectCondition.valueOf(integerTest);
        printAllMatching(conditionString, new FieldFilteringIterator(dataObjectIterator, condition));

    }


    protected static void printAllMatching(String conditionString, Iterator<DataObject> matching) {
        System.out.println("condition matched by the following records: " + conditionString);
        while (matching.hasNext()) {
            System.out.println(matching.next().toJSONString(false));
        }
    }

//
//    /**
//     * Test of matches method, of class KeywordsCondition.
//     */
//    public void testMatches() {
//        ObjectStringSet objectStringSet = new ObjectStringSet("adin", keywordsData1);
//        MetaObjectFixedMap metaObjectOne = new MetaObjectFixedMap("keywords", objectStringSet, "adin");
//        ObjectStringSet objectStringSet2 = new ObjectStringSet("dva", keywordsData2);
//        MetaObjectFixedMap metaObjectTwo = new MetaObjectFixedMap("keywords", objectStringSet2, "dva");
//        ObjectStringSet objectStringSet12 = new ObjectStringSet("jednadva", keywordsData12);
//        MetaObjectFixedMap metaObjectOneAndTwo = new MetaObjectFixedMap("keywords", objectStringSet12, "jednadva");
//
//
//        SelectionCondition<MetaObject> conditionOne = new SingleFieldCondition("keywords", KeywordsCondition.valueOf(keywordStringOne));
//        SelectionCondition<MetaObject> conditionTwo = new SingleFieldCondition("keywords", KeywordsCondition.valueOf(keywordStringTwo));
//        SelectionCondition<MetaObject> conditionOneAndTwo = new SingleFieldCondition("keywords", KeywordsCondition.valueOf(keywordStringOneAndTwo));
//
//
//        assertEquals(true, conditionOne.matches(metaObjectOne));
//        assertEquals(false, conditionTwo.matches(metaObjectOne));
//        assertEquals(false, conditionOneAndTwo.matches(metaObjectOne));
//
//        assertEquals(false, conditionOne.matches(metaObjectTwo));
//        assertEquals(true, conditionTwo.matches(metaObjectTwo));
//        assertEquals(false, conditionOneAndTwo.matches(metaObjectTwo));
//
//        assertEquals(true, conditionOne.matches(metaObjectOneAndTwo));
//        assertEquals(true, conditionTwo.matches(metaObjectOneAndTwo));
//        assertEquals(true, conditionOneAndTwo.matches(metaObjectOneAndTwo));
//    }
//
//    public void testWholeDataCondition() {
//        ObjectStringSet objectStringSet = new ObjectStringSet("adin", keywordsData1);
//        MetaObjectFixedMap metaObjectOne = new MetaObjectFixedMap("keywords", objectStringSet, "adin");
//        ObjectStringSet objectStringSet2 = new ObjectStringSet("dva", keywordsData2);
//        MetaObjectFixedMap metaObjectTwo = new MetaObjectFixedMap("keywords", objectStringSet2, "dva");
//        ObjectStringSet objectStringSet12 = new ObjectStringSet("jednadva", keywordsData12);
//        MetaObjectFixedMap metaObjectOneAndTwo = new MetaObjectFixedMap("keywords", objectStringSet12, "jednadva");
//
//        KeywordsCondition condOne = KeywordsCondition.valueOf(keywordStringOne);
//        KeywordsCondition condTwo = KeywordsCondition.valueOf(keywordStringTwo);
//        KeywordsCondition condOneAndTwo = KeywordsCondition.valueOf(keywordStringOneAndTwo);
//
//        KNNQueryByLocatorOperation operation = new KNNQueryByLocatorOperation(new String [] {"locator"}, 30);
//        operation.setParameter("FILTER_keywords", condOne);
//        operation.setParameter("FILTER_xxx", null);
//        SelectionCondition<MetaObject> conditionOne = DataObjectCondition.valueOf(operation);
//
//        operation = new KNNQueryByLocatorOperation(new String [] {"locator"}, 30);
//        operation.setParameter("FILTER_keywords", condTwo);
//        operation.setParameter("FILTER_", condOne);
//        SelectionCondition<MetaObject> conditionTwo = DataObjectCondition.valueOf(operation);
//
//        operation = new KNNQueryByLocatorOperation(new String [] {"locator"}, 30);
//        operation.setParameter("FILTER_keywords", condOneAndTwo);
//        SelectionCondition<MetaObject> conditionOneAndTwo = DataObjectCondition.valueOf(operation);
//
//        assertEquals(true, conditionOne.matches(metaObjectOne));
//        assertEquals(false, conditionTwo.matches(metaObjectOne));
//        assertEquals(false, conditionOneAndTwo.matches(metaObjectOne));
//
//        assertEquals(false, conditionOne.matches(metaObjectTwo));
//        assertEquals(true, conditionTwo.matches(metaObjectTwo));
//        assertEquals(false, conditionOneAndTwo.matches(metaObjectTwo));
//
//        assertEquals(true, conditionOne.matches(metaObjectOneAndTwo));
//        assertEquals(true, conditionTwo.matches(metaObjectOneAndTwo));
//        assertEquals(true, conditionOneAndTwo.matches(metaObjectOneAndTwo));
//    }
//
//   /**
//     * Test of matches method, of class KeywordsCondition.
//     */
//    public void testBooleanAttributeMatch() {
//        ObjectBooleanAttribute attributeTrue = new ObjectBooleanAttribute("yes", true);
//        ObjectStringSet objectStringSet = new ObjectStringSet("adin", keywordsData1);
//        MetaObjectFixedMap metaObjectOne = new MetaObjectFixedMap(new String [] {"keywords", "nude"}, new LocalAbstractObject[] {objectStringSet, attributeTrue}, "adin");
//
//
//        Map<String,Object> conditions = new HashMap<>();
//        conditions.put(SelectionCondition.paramPrefix + "keywords", KeywordsCondition.valueOf(keywordStringOne));
//        conditions.put(SelectionCondition.paramPrefix + "nude", BooleanAttCondition.valueOf("true,false"));
//        ParametricBase conditionsParametric = new ParametricBase(conditions);
//        SelectionCondition<MetaObject> conditionOneTrue = DataObjectCondition.valueOf(conditionsParametric);
//
//        conditions.clear();
//        conditions.put(SelectionCondition.paramPrefix + "keywords", KeywordsCondition.valueOf(keywordStringTwo));
//        conditions.put(SelectionCondition.paramPrefix + "nude", BooleanAttCondition.valueOf("true,false"));
//        conditionsParametric = new ParametricBase(conditions);
//        SelectionCondition<MetaObject> conditionTwoTrue = DataObjectCondition.valueOf(conditionsParametric);
//
//
//        assertEquals(true, conditionOneTrue.matches(metaObjectOne));
//        assertEquals(false, conditionTwoTrue.matches(metaObjectOne));
//    }
//
//   /**
//     * Test of matches method, of class KeywordsCondition.
//     */
//    public void testKWAndBool() {
//        ObjectBooleanAttribute attributeTrue = new ObjectBooleanAttribute("yes", true);
//        MetaObjectFixedMap metaObjectYes = new MetaObjectFixedMap("nude", attributeTrue, "yes");
//        ObjectBooleanAttribute attributeFalse = new ObjectBooleanAttribute("no", false);
//        MetaObjectFixedMap metaObjectNo = new MetaObjectFixedMap("nude", attributeFalse, "no");
//        MetaObjectFixedMap metaObjectEmpty = new MetaObjectFixedMap("xxx", null, "empty");
//
//
//        SelectionCondition<MetaObject> conditionTrueFalse = new SingleFieldCondition("nude", BooleanAttCondition.valueOf("true,false"));
//        SelectionCondition<MetaObject> conditionTrueTrue = new SingleFieldCondition("nude", BooleanAttCondition.valueOf("true ,true"));
//        SelectionCondition<MetaObject> conditionFalseTrue = new SingleFieldCondition("nude", BooleanAttCondition.valueOf("false, true"));
//        SelectionCondition<MetaObject> conditionFalseFalse = new SingleFieldCondition("nude", BooleanAttCondition.valueOf("false , false"));
//        SelectionCondition<MetaObject> conditionTrue = new SingleFieldCondition("nude", BooleanAttCondition.valueOf("true"));
//
//
//        assertEquals(true, conditionTrueFalse.matches(metaObjectYes));
//        assertEquals(false, conditionTrueFalse.matches(metaObjectNo));
//        assertEquals(false, conditionTrueFalse.matches(metaObjectEmpty));
//    }
//
//   /**
//     * Test of matches method, of class KeywordsCondition.
//     */
//    public void testValueMatch() {
//        ObjectBooleanAttribute attributeTrue = new ObjectBooleanAttribute(null, true);
//        ObjectString attributeDavid = new ObjectString("david");
//        ObjectString attributeMichal = new ObjectString("michal");
//        MetaObjectFixedMap metaObjectDavid = new MetaObjectFixedMap(new String [] {"user", "nude"}, new LocalAbstractObject[] {attributeDavid, attributeTrue}, "adin");
//        MetaObjectFixedMap metaObjectMichal = new MetaObjectFixedMap(new String [] {"user", "nude"}, new LocalAbstractObject[] {attributeMichal, attributeTrue}, "adin");
//        MetaObjectFixedMap metaObjectEmpty = new MetaObjectFixedMap(new String [] {"nude"}, new LocalAbstractObject[] {attributeTrue}, "adin");
//
//
//        Map<String,Object> conditions = new HashMap<>();
//        conditions.put(SelectionCondition.paramPrefix + "user", ValueMatchCondition.valueOf("david:true"));
//        ParametricBase conditionsParametric = new ParametricBase(conditions);
//        SelectionCondition<MetaObject> conditionDavidTrue = DataObjectCondition.valueOf(conditionsParametric);
//
//        conditions.clear();
//        conditions.put(SelectionCondition.paramPrefix + "user", ValueMatchCondition.valueOf("michal:false"));
//        conditionsParametric = new ParametricBase(conditions);
//        SelectionCondition<MetaObject> conditionMichalFalse = DataObjectCondition.valueOf(conditionsParametric);
//
//
//        assertEquals(true, conditionDavidTrue.matches(metaObjectDavid));
//        assertEquals(false, conditionDavidTrue.matches(metaObjectMichal));
//        assertEquals(false, conditionDavidTrue.matches(metaObjectEmpty));
//
//        assertEquals(true, conditionMichalFalse.matches(metaObjectDavid));
//        assertEquals(false, conditionMichalFalse.matches(metaObjectMichal));
//        assertEquals(true, conditionMichalFalse.matches(metaObjectEmpty));
//    }
//    /**
//     * Test of valueOf method, of class KeywordsCondition.
//     */
//    public void testValueOf() {
//        KeywordsCondition condition = KeywordsCondition.valueOf(keywordString1);
//        System.out.println("condition parsed: ");
//        for (String[] set : condition.getKeywords()) {
//            System.out.print(Arrays.toString(set)+" ");
//        }
//    }
    
}
