/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package data;

import junit.framework.TestCase;
import messif.data.processing.KeywordsPreprocessor;
import messif.record.Record;
import messif.record.processing.RecordProcessor;
import messif.utility.json.JSONReader;

/**
 *
 * @author david
 */
public class ObjectStringSetTest extends TestCase {
    
    public final static String data1 = "{ \"something\": 12, \"keywords\": [ \"One\", \"two\", \"three\", \"four\", \"five\" ] }";

    public final static RecordProcessor keywordModifier = KeywordsPreprocessor.create("keywords", "keywords_internal");

    public ObjectStringSetTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Test of parseStringList method, of class ObjectStringSet.
     */
//    public void testParseStringList() throws Exception {
//        String[] parseStringList = ObjectStringSet.parseStringList(data1);
//        printWordsInfo(parseStringList);
//        
//        parseStringList = ObjectStringSet.parseStringList(data2);
//        printWordsInfo(parseStringList);   
//    }

    public void testConstructor() throws Exception {
        final Object o = JSONReader.readObjectFrom(data1, true);
        if (! (o instanceof Record)) {
            fail("error parsing JSON: " + data1);
        }
        Record record = (Record) o;
        System.out.println(record.toJSONString(true));
        record = keywordModifier.process(record);
        System.out.println(record.toJSONString(true));
    }
    
    private static void printWordsInfo(Record record) {

    }

}
