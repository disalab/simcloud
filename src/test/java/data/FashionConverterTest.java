/*
 *  This file is part of SimCloud library: https://bitbucket.org/disalab/simcloud
 *
 *  SimCloud library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SimCloud library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SimCloud library.  If not, see <http://www.gnu.org/licenses/>.
 */
package data;

import junit.framework.TestCase;
import messif.data.DataObject;
import messif.data.processing.FashionDataConverter;
import messif.record.processing.RecordProcessingException;
import messif.utility.json.JSONReader;

/**
 *
 * @author david
 */
public class FashionConverterTest extends TestCase {
    
    public FashionConverterTest(String testName) {
        super(testName);
        matrix = new float[][] { { 1f, 2f}, {2f, 3f}, {3f, 4f} };
    }

    final float [] [] matrix;

    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Test of convert method, of class FashionConverter.
     */
//    public void testConvert() throws Exception {
//        System.out.println("convert");
//        ObjectFloatVectorNeuralNetwork value = null;
//        FashionConverter instance = null;
//        ObjectFloatVectorL2 expResult = null;
//        ObjectFloatVectorL2 result = instance.convert(value);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    public final static String data1 = "{ \"something\": 12, \"original\": [ 1.0, 2.0, 3.0 ] }";

    /**
     * Test of getDestinationClass method, of class FashionConverter.
     */
    public void testMatrixMult() throws InstantiationException, RecordProcessingException {
        final FashionDataConverter fashionDataConverter = new FashionDataConverter(matrix, "original", "compressed", false);

        final Object o = JSONReader.readObjectFrom(data1, true);
        if (! (o instanceof DataObject)) {
            fail("error parsing JSON: " + data1);
        }
        DataObject record = (DataObject) o;

        System.out.println(record.toJSONString(true));
        record = fashionDataConverter.process(record);
        System.out.println(record.toJSONString(true));
    }
    
}
