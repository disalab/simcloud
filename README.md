# README #

The SimCloud is a Java library that can integrate one or more similarity indexes with an ID-object store (and potentially more indexes). It has been used in demo described in the SIGIR paper

D. Novak, M. Batko, and P. Zezula, “Large-scale Image Retrieval using Neural Net Descriptors,” in Proceedings of SIGIR ’15, 2015.

If you use the library for academic purposes, please reference this paper in your work.

### Who do I talk to? ###

* David Novak <david.novak(at)fi.muni.cz>


### Licence ###

SimCloud library is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. MESSIF library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.